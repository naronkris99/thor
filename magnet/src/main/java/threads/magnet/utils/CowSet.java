package threads.magnet.utils;

import androidx.annotation.NonNull;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class CowSet<Key> {

    private final Map<Key, Boolean> backingStore = new ConcurrentHashMap<>();

    public boolean isEmpty() {
        return backingStore.isEmpty();
    }

    public boolean contains(Key o) {
        return backingStore.containsKey(o);
    }


    public boolean add(Key e) {
        if (backingStore.containsKey(e))
            return false;
        return backingStore.putIfAbsent(e, Boolean.TRUE) == null;
    }


    public void remove(Key o) {
        if (!backingStore.containsKey(o))
            return;
        backingStore.remove(o);
    }


    public void addAll(@NonNull Collection<? extends Key> c) {
        for (Key e : c) {
            add(e);
        }
    }

    @NonNull
    public Stream<Key> stream() {
        return backingStore.keySet().stream();
    }

    public Set<Key> snapshot() {
        return Collections.unmodifiableSet(backingStore.keySet());
    }

}
