package threads.thor;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.DocumentsContract;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import threads.lite.IPFS;
import threads.thor.core.events.EVENTS;
import threads.thor.provider.DataProvider;
import threads.thor.services.MimeTypeService;
import threads.thor.utils.AdBlocker;

public class InitApplication extends Application {

    public static final String TIME_TAG = "TIME_TAG";
    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    private static final String TAG = InitApplication.class.getSimpleName();

    @NonNull
    public static Intent getDownloadsIntent() {
        Uri uri = DataProvider.getDownloadsUri();
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType(MimeTypeService.ALL);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        return intent;
    }

    private void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            threads.lite.LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        long start = System.currentTimeMillis();

        createStorageChannel(getApplicationContext());

        AdBlocker.init(getApplicationContext());

        EVENTS events = EVENTS.getInstance(getApplicationContext());

        LogUtils.info(TIME_TAG, "InitApplication after add blocker [" +
                (System.currentTimeMillis() - start) + "]...");
        try {
            IPFS.getInstance(getApplicationContext());
        } catch (Throwable throwable) {
            events.fatal(getString(R.string.fatal_error,
                    throwable.getClass().getSimpleName(),
                    "" + throwable.getMessage()));
            LogUtils.error(TAG, throwable);
        }

        LogUtils.info(TIME_TAG, "InitApplication after starting ipfs [" +
                (System.currentTimeMillis() - start) + "]...");


    }
}