package threads.thor.core.pages;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;

@androidx.room.Entity
public class Page {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private final String id;
    @Nullable
    @ColumnInfo(name = "cid")
    @TypeConverters(Cid.class)
    private Cid cid;
    @ColumnInfo(name = "sequence")
    private long sequence;

    public Page(@NonNull String id) {
        this.id = id;
        this.sequence = 0L;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public Cid getCid() {
        return cid;
    }

    public void setCid(@NonNull Cid cid) {
        this.cid = cid;
    }


}