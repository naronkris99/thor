package threads.lite.ident;

import java.nio.ByteBuffer;
import java.util.Set;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.host.LiteHost;
import threads.lite.utils.DataHandler;

public class IdentityHandler implements ProtocolHandler {

    private final LiteHost host;
    private final Session session;

    public IdentityHandler(LiteHost host, Session session) {
        this.host = host;
        this.session = session;
    }

    @Override
    public String getProtocol() {
        return IPFS.IDENTITY_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PROTOCOL));

        Set<String> set = session.getProtocols().keySet();
        IdentifyOuterClass.Identify response =
                host.createIdentity(set, host.listenAddresses(),
                        stream.getConnection().remoteMultiaddr());
        stream.writeOutput(DataHandler.encode(response))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        throw new Exception("should not be invoked");
    }
}
