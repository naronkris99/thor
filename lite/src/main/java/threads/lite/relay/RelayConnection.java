package threads.lite.relay;

import androidx.annotation.NonNull;

import net.luminis.quic.Version;

import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteHost;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.utils.DataHandler;

public class RelayConnection implements Connection {

    // stream handlers are cleared during close connection
    private final ConcurrentHashMap<Integer, StreamHandler> streamHandlers =
            new ConcurrentHashMap<>();

    private final AtomicBoolean initMode = new AtomicBoolean(true);
    private final AtomicReference<Connection> connection = new AtomicReference<>();
    private final AtomicReference<MuxedTransport> transport = new AtomicReference<>();
    private final AtomicReference<Reservation.Kind> kind =
            new AtomicReference<>(Reservation.Kind.LIMITED);
    private final AtomicBoolean upgraded = new AtomicBoolean(false);
    private final LiteHost host;
    private final PeerId peerId;
    private final PeerId relayId;
    private final Multiaddr observed;
    private final Session session;
    private final Parameters parameters;
    private final Server server;
    private Consumer<Connection> upgrade;
    private Consumer<Throwable> throwable;

    public RelayConnection(Connection connection, LiteHost host, Session session, PeerId peerId,
                           PeerId relayId, Multiaddr observed, Parameters parameters, Server server) {
        this.connection.set(connection);
        this.host = host;
        this.session = session;
        this.peerId = peerId;
        this.relayId = relayId;
        this.observed = observed;
        this.parameters = parameters;
        this.server = server;
    }


    public Server getServer() {
        return server;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public LiteHost getHost() {
        return host;
    }

    public PeerId getPeerId() {
        return peerId;
    }

    public Session getSession() {
        return session;
    }

    // returns true if it is a static relay
    public boolean isStaticRelay() {
        return kind.get() == Reservation.Kind.STATIC;
    }

    @NonNull
    public Multiaddr getObserved() {
        return observed;
    }

    @Override
    public void close() {
        connection.get().close();
        streamHandlers.clear();
        server.shutdown();
    }

    @Override
    public boolean isConnected() {
        return connection.get().isConnected();
    }

    @NonNull
    @Override
    public InetSocketAddress getRemoteAddress() {
        return connection.get().getRemoteAddress();
    }

    @NonNull
    @Override
    public PeerId remotePeerId() {
        return peerId;
    }

    public void upgradeConnection(Connection connection) {
        Objects.requireNonNull(connection);
        this.connection.getAndSet(connection).close();
        this.upgraded.set(true);
        Objects.requireNonNull(this.upgrade);
        this.upgrade.accept(connection);
    }

    @NonNull
    public StreamHandler getStreamHandler(int streamId) {
        return Objects.requireNonNull(streamHandlers.get(streamId));
    }

    // this will return either a direct connection or in case a static relay is detected,
    // it will return the static relay (only when a direct connection failed)
    @NonNull
    public Connection upgradeConnection() throws ConnectException, InterruptedException {
        CompletableFuture<Connection> done = new CompletableFuture<>();

        this.upgrade = done::complete;
        this.throwable = done::completeExceptionally;

        connection.get().createStream(new RelayHandler(this))
                .whenComplete((stream, throwable) -> {
                    if (throwable != null) {
                        done.completeExceptionally(throwable);
                    } else {
                        stream.writeOutput(DataHandler.encodeProtocols(
                                IPFS.MULTISTREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP));
                    }
                });
        try {
            return done.get(IPFS.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        } catch (TimeoutException | ExecutionException exception) {
            if (isStaticRelay()) {
                return this;
            } else {
                this.close();
            }
            throw new ConnectException(exception.getMessage());
        } catch (InterruptedException interruptedException) {
            close();
            throw interruptedException;
        }
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> createStream(@NonNull StreamHandler streamHandler) {
        if (upgraded.get()) {
            return connection.get().createStream(streamHandler);
        } else {
            CompletableFuture<Stream> done = new CompletableFuture<>();
            initMode.set(false);
            MuxedTransport muxedTransport = transport.get();
            Objects.requireNonNull(muxedTransport);
            MuxedStream.createStream(muxedTransport)
                    .whenComplete((muxedStream, throwable1) -> {
                        if (throwable1 != null) {
                            done.completeExceptionally(throwable1);
                        } else {
                            streamHandlers.put(muxedStream.getStreamId(), streamHandler);
                            done.complete(muxedStream);
                        }
                    });
            return done;
        }
    }

    @NonNull
    @Override
    public Multiaddr remoteMultiaddr() {
        if (upgraded.get()) {
            return connection.get().remoteMultiaddr();
        } else {
            return Multiaddr.createCircuit(connection.get().getVersion(),
                    peerId, relayId, connection.get().getRemoteAddress());
        }
    }

    @Override
    public void keepAlive(int pingInterval) throws Exception {
        connection.get().keepAlive(pingInterval);

    }

    @Override
    public Version getVersion() {
        return connection.get().getVersion();
    }

    @Override
    public int getSmoothedRtt() {
        return connection.get().getSmoothedRtt();
    }

    public void setLimit(Reservation.Limit limit) {
        kind.set(limit.getKind()); // for now only the kind is used
    }

    public void upgradeTransport(@NonNull MuxedTransport transport) {
        this.transport.set(transport);
    }

    public boolean initMode() {
        return initMode.get();
    }

    public void throwable(@NonNull Throwable throwable) {
        Objects.requireNonNull(this.throwable);
        this.throwable.accept(throwable);
    }
}
