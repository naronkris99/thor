package threads.lite.relay;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteServer;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;

public class RelayService {


    @NonNull
    public static RelayConnection createRelayConnection(@NonNull LiteHost host,
                                                        @NonNull Session session,
                                                        @NonNull Multiaddr address,
                                                        @NonNull Parameters parameters)
            throws Exception {

        PeerId relayId = address.getRelayId();
        Objects.requireNonNull(relayId);
        PeerId peerId = address.getPeerId();
        Multiaddr relayAddress = address.getRelayAddress();

        final AtomicReference<RelayConnection> relayConnection = new AtomicReference<>();
        LiteServer server = host.createServer(
                new DatagramSocket(), connection -> {
                    if (Objects.equals(connection.remotePeerId(), peerId)) {
                        // the right peer has connected
                        RelayConnection relay = relayConnection.getAndSet(null);
                        if (relay != null) {
                            relay.upgradeConnection(connection);
                        }
                    }
                }, connection -> {
                }, peerId1 -> false);
        try {

            Connection connection = server.connect(server.getServerSession().getStreamHandler(),
                    relayAddress,
                    Parameters.getDefaultUniDirection(IPFS.GRACE_PERIOD, IPFS.MESSAGE_SIZE_MAX));

            PeerInfo peerInfo = IdentityService.getPeerInfo(host, connection)
                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!peerInfo.hasRelayHop()) {
                connection.close();
                throw new ConnectException("No relay hop protocol");
            }
            Multiaddr observed = peerInfo.getObserved();
            if (observed.isCircuitAddress()) {
                throw new ConnectException("Observed address is circuit address");
            }
            relayConnection.set(new RelayConnection(connection, host, session, peerId,
                    relayId, observed, parameters, server));
            return relayConnection.get();
        } catch (TimeoutException | ExecutionException exception) {
            server.shutdown();
            throw new ConnectException(exception.getMessage());
        } catch (Throwable throwable) {
            server.shutdown();
            throw throwable;
        }
    }

    @NonNull
    public static CompletableFuture<Reservation> reservation(
            @NonNull LiteHost host, @NonNull Server server,
            @NonNull Multiaddr multiaddr) {

        CompletableFuture<Reservation> reservationFuture = new CompletableFuture<>();
        AtomicReference<Reservation.Kind> reservationKind =
                new AtomicReference<>(Reservation.Kind.LIMITED);
        AtomicReference<Multiaddr> observed = new AtomicReference<>();
        try {
            Connection connection = server.connect(new RelayResponder(host, server) {
                                                       @NonNull
                                                       @Override
                                                       public Multiaddr getObserved() {
                                                           return Objects.requireNonNull(observed.get());
                                                       }

                                                       @NonNull
                                                       @Override
                                                       public Reservation.Kind getReservationKind() {
                                                           return reservationKind.get();
                                                       }
                                                   },
                    multiaddr, new Parameters(IPFS.GRACE_PERIOD_RESERVATION,
                            IPFS.MESSAGE_SIZE_MAX, IPFS.MAX_STREAMS));

            IdentityService.getPeerInfo(host, connection).whenComplete((peerInfo, throwable) -> {
                if (throwable != null) {
                    reservationFuture.completeExceptionally(throwable);
                } else {

                    if (!peerInfo.hasRelayHop()) {
                        reservationFuture.completeExceptionally(new Exception("no relay hop"));
                        return;
                    }
                    observed.set(peerInfo.getObserved());

                    getReservation(host, connection, multiaddr)
                            .whenComplete((reservation, throwable1) -> {
                                if (throwable1 != null) {
                                    reservationFuture.completeExceptionally(throwable1);
                                } else {
                                    reservationKind.set(reservation.getKind());
                                    reservationFuture.complete(reservation);
                                }
                            });

                }
            });
        } catch (Throwable throwable) {
            reservationFuture.completeExceptionally(throwable);
        }
        return reservationFuture;
    }


    public static CompletableFuture<Reservation> getReservation(
            @NonNull LiteHost host, @NonNull Connection connection,
            @NonNull Multiaddr relayAddress) {


        CompletableFuture<Reservation> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                LogUtils.error(TAG, throwable);
                stream.getConnection().close();
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {

                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                        IPFS.RELAY_PROTOCOL_HOP).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }

                if (Objects.equals(protocol, IPFS.RELAY_PROTOCOL_HOP)) {
                    Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                            .setType(Circuit.HopMessage.Type.RESERVE).build();
                    stream.writeOutput(DataHandler.encode(message))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
                    if (msg.getStatus() == Circuit.Status.OK) {

                        if (msg.hasReservation()) {
                            Circuit.Reservation reserve = msg.getReservation();
                            Reservation.Limit limit = getReservationLimit(msg);
                            Reservation reservation = new Reservation(host, limit,
                                    stream.getConnection(), relayAddress, reserve);

                            for (Multiaddr addr : reservation.getAddresses()) {
                                LogUtils.error(TAG, "ipfs swarm connect " + addr);
                            }

                            connection.keepAlive(IPFS.GRACE_PERIOD);

                            done.complete(reservation);
                        } else {
                            throwable(stream, new Exception("NO RESERVATION"));
                        }

                    } else {
                        throwable(stream, new Exception(msg.getStatus().toString()));
                    }
                } else {
                    throwable(stream, new Exception("NO RESERVATION"));
                }
            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(DataHandler.encodeProtocols(
                        IPFS.MULTISTREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP));
            }
        });

        return done;
    }


    public static Reservation.Limit getReservationLimit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        long limitDuration = 0;
        Reservation.Kind kind = Reservation.Kind.LIMITED;
        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            if (limit.hasData()) {
                limitData = limit.getData();
            }
            if (limit.hasDuration()) {
                limitDuration = limit.getDuration();
            }
        } else {
            kind = Reservation.Kind.STATIC;
        }

        return new Reservation.Limit(limitData, limitDuration, kind);
    }
}
