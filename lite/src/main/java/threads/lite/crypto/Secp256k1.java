package threads.lite.crypto;

import androidx.annotation.NonNull;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

import crypto.pb.Crypto;

public class Secp256k1 {

    public static PubKey unmarshalSecp256k1PublicKey(byte[] keyBytes) throws Exception {

        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(keyBytes));
        return new Secp256k1PublicKey(publicKey);

    }


    public static final class Secp256k1PublicKey extends PubKey {
        private final PublicKey publicKey;

        public Secp256k1PublicKey(PublicKey publicKey) {
            super(Crypto.KeyType.Secp256k1);
            this.publicKey = publicKey;
        }

        public void verify(byte[] data, byte[] signature) throws Exception {
            Signature ecdsaVerify = Signature.getInstance("SHA256withECDSA");
            ecdsaVerify.initVerify(publicKey);
            ecdsaVerify.update(data);
            boolean result = ecdsaVerify.verify(signature);
            if (!result) {
                throw new Exception("verify failed");
            }
        }

        public int hashCode() {
            return this.publicKey.hashCode();
        }

        @NonNull
        public byte[] raw() {
            return this.publicKey.getEncoded();
        }
    }

}
