package threads.lite.utils;

import java.nio.ByteBuffer;

import threads.lite.IPFS;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;

public class MultistreamHandler implements ProtocolHandler {
    @Override
    public String getProtocol() {
        return IPFS.MULTISTREAM_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL));
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        throw new Exception("should not be invoked");
    }
}
