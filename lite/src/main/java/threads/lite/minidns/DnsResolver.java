package threads.lite.minidns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.minidns.record.A;
import threads.lite.minidns.record.AAAA;
import threads.lite.minidns.record.Data;
import threads.lite.minidns.record.Record;
import threads.lite.minidns.record.TXT;


public final class DnsResolver {
    public static final String DNS_ADDR = "dnsaddr=";
    public static final String DNS_LINK = "dnslink=";
    public static final Set<Inet4Address> STATIC_IPV4_DNS_SERVERS = new CopyOnWriteArraySet<>();
    public static final Set<Inet6Address> STATIC_IPV6_DNS_SERVERS = new CopyOnWriteArraySet<>();
    private static final String TAG = DnsResolver.class.getSimpleName();

    static {
        try {
            Inet4Address googleV4Dns = DnsUtility.ipv4From(IPFS.MINIDNS_DNS_SERVER_IP4);
            STATIC_IPV4_DNS_SERVERS.add(googleV4Dns);
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv4 DNS Server " + e.getMessage());
        }

        try {
            Inet6Address googleV6Dns = DnsUtility.ipv6From(IPFS.MINIDNS_DNS_SERVER_IP6);
            STATIC_IPV6_DNS_SERVERS.add(googleV6Dns);
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv6 DNS Server " + e.getMessage());
        }
    }

    @NonNull
    public static String resolveDnsLink(@NonNull DnsClient client, @NonNull String host) {

        Set<String> txtRecords = retrieveTxtRecords(client, "_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_LINK)) {
                    return txtRecord.replaceFirst(DNS_LINK, "");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, host + " " + throwable.getClass().getName());
            }
        }
        return "";
    }

    @NonNull
    private static InetAddress resolveAddress(@NonNull DnsClient client,
                                              @NonNull String host,
                                              boolean ipv6) throws UnknownHostException {
        try {
            if (ipv6) {
                DnsQueryResult result = client.query(host, Record.TYPE.AAAA);
                DnsMessage response = result.response;
                List<Record<? extends Data>> records = response.answerSection;
                for (Record<? extends Data> record : records) {
                    Data payload = record.getPayload();
                    if (payload instanceof AAAA) {
                        AAAA a = (AAAA) payload;
                        return a.getInetAddress();
                    }
                }
            } else {
                DnsQueryResult result = client.query(host, Record.TYPE.A);
                DnsMessage response = result.response;
                List<Record<? extends Data>> records = response.answerSection;
                for (Record<? extends Data> record : records) {
                    Data payload = record.getPayload();
                    if (payload instanceof A) {
                        A a = (A) payload;
                        return a.getInetAddress();
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return InetAddress.getByName(host);
    }

    @NonNull
    private static Set<String> retrieveTxtRecords(@NonNull DnsClient client,
                                                  @NonNull String host) {
        Set<String> txtRecords = new HashSet<>();
        try {
            DnsQueryResult result = client.query(host, Record.TYPE.TXT);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof TXT) {
                    TXT text = (TXT) payload;
                    txtRecords.add(text.getText());
                } else {
                    LogUtils.warning(TAG, payload.toString());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return txtRecords;
    }


    @Nullable
    private static Multiaddr createMultiaddr(@NonNull DnsClient dnsClient,
                                             @NonNull Multiaddr multiaddress,
                                             boolean ipv6) {
        try {
            String host = multiaddress.getHost();
            InetAddress address = resolveAddress(dnsClient, host, ipv6);
            Objects.requireNonNull(address);
            return Multiaddr.upgrade(multiaddress, address);
        } catch (Throwable ignore) {
            LogUtils.error(TAG, multiaddress + " not supported");
        }
        return null;
    }


    @Nullable
    public static Multiaddr resolveDns(@NonNull DnsClient dnsClient, @NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns6()) {
            return createMultiaddr(dnsClient, multiaddr, true);
        }
        if (multiaddr.isDns4()) {
            return createMultiaddr(dnsClient, multiaddr, false);
        }
        if (multiaddr.isDns()) {
            return createMultiaddr(dnsClient, multiaddr, false);
        }
        throw new IllegalStateException("not a dns multiaddress");
    }

    @NonNull
    public static List<Multiaddr> resolveDnsaddr(@NonNull DnsClient dnsClient,
                                                 @NonNull IPV ipv,
                                                 @NonNull Multiaddr multiaddr) {
        List<Multiaddr> multiaddrs = new ArrayList<>();
        if (!multiaddr.isDnsaddr()) {
            return multiaddrs;
        }
        String host = multiaddr.getHost();
        Objects.requireNonNull(host);

        try {
            PeerId peerId = multiaddr.getPeerId();
            Set<Multiaddr> addresses = resolveDnsaddrHost(dnsClient, ipv, host);
            for (Multiaddr addr : addresses) {
                PeerId cmpPeerId = addr.getPeerId();
                if (Objects.equals(cmpPeerId, peerId)) {
                    multiaddrs.add(addr);
                }
            }
        } catch (Throwable ignore) {
        }
        return multiaddrs;
    }


    @NonNull
    public static Set<Multiaddr> resolveDnsaddrHost(@NonNull DnsClient dnsClient,
                                                    @NonNull IPV ipv,
                                                    @NonNull String host) {
        return resolveDnsAddressInternal(dnsClient, ipv, host, new HashSet<>());
    }

    @NonNull
    public static Set<Multiaddr> resolveDnsAddressInternal(@NonNull DnsClient dnsClient,
                                                           @NonNull IPV ipv,
                                                           @NonNull String host,
                                                           @NonNull Set<String> hosts) {
        Set<Multiaddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        Set<String> txtRecords = retrieveTxtRecords(dnsClient, "_dnsaddr." + host);

        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    Multiaddr multiaddr = Multiaddr.create(testRecordReduced);
                    if (multiaddr.isDnsaddr()) {
                        String childHost = multiaddr.getHost();
                        multiAddresses.addAll(resolveDnsAddressInternal(dnsClient, ipv,
                                childHost, hosts));
                    } else {

                        if (multiaddr.protocolSupported(ipv, true)) {
                            if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
                                Multiaddr resolved = resolveDns(dnsClient, multiaddr);
                                if (resolved != null) {
                                    multiAddresses.add(resolved);
                                }
                            } else {
                                multiAddresses.add(multiaddr);
                            }
                        }

                    }
                }
            } catch (Throwable throwable) {
                LogUtils.warning(TAG, "Not supported " + txtRecord);
            }
        }
        return multiAddresses;
    }

    @NonNull
    public static DnsClient getInstance(@NonNull Supplier<List<InetAddress>> settingSupplier) {
        return new DnsClient(settingSupplier, new DnsCache(128));
    }

}
