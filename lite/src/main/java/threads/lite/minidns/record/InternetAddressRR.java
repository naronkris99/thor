/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.record;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * A resource record representing a internet address. Provides {@link #getInetAddress()}.
 */
public abstract class InternetAddressRR<IA extends InetAddress> extends Data {


    /**
     * Target IP.
     */
    protected final byte[] ip;

    /**
     * Cache for the {@link InetAddress} this record presents.
     */
    private transient IA inetAddress;

    protected InternetAddressRR(byte[] ip) {
        this.ip = ip;
    }

    protected InternetAddressRR(IA inetAddress) {
        this(inetAddress.getAddress());
        this.inetAddress = inetAddress;
    }

    @Override
    public final void serialize(DataOutputStream dos) throws IOException {
        dos.write(ip);
    }

    @SuppressWarnings("unchecked")
    public final IA getInetAddress() {
        if (inetAddress == null) {
            try {
                inetAddress = (IA) InetAddress.getByAddress(ip);
            } catch (UnknownHostException e) {
                throw new IllegalStateException(e);
            }
        }
        return inetAddress;
    }
}
