/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.edns;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import threads.lite.minidns.DnsName;
import threads.lite.minidns.record.Data;
import threads.lite.minidns.record.OPT;
import threads.lite.minidns.record.Record;
import threads.lite.minidns.record.Record.TYPE;

/**
 * EDNS - Extension Mechanism for DNS.
 *
 * @see <a href="https://tools.ietf.org/html/rfc6891">RFC 6891 - Extension Mechanisms for DNS (EDNS(0))</a>
 */
public class Edns {

    /**
     * Inform the dns server that the client supports DNSSEC.
     */
    public static final int FLAG_DNSSEC_OK = 0x8000;
    public final int udpPayloadSize;
    /**
     * 8-bit extended return code.
     * <p>
     * RFC 6891 § 6.1.3 EXTENDED-RCODE
     */
    public final int extendedRcode;
    /**
     * 8-bit version field.
     * <p>
     * RFC 6891 § 6.1.3 VERSION
     */
    public final int version;
    /**
     * 16-bit flags.
     * <p>
     * RFC 6891 § 6.1.4
     */
    public final int flags;
    public final List<EdnsOption> variablePart;
    public final boolean dnssecOk;
    private Record<OPT> optRecord;
    private String terminalOutputCache;

    public Edns(Record<OPT> optRecord) {
        assert optRecord.type == TYPE.OPT;
        udpPayloadSize = optRecord.clazzValue;
        extendedRcode = (int) ((optRecord.ttl >> 8) & 0xff);
        version = (int) ((optRecord.ttl >> 16) & 0xff);
        flags = (int) optRecord.ttl & 0xffff;

        dnssecOk = (optRecord.ttl & FLAG_DNSSEC_OK) > 0;

        OPT opt = optRecord.payloadData;
        variablePart = opt.variablePart;
        this.optRecord = optRecord;
    }

    public Edns(Builder builder) {
        udpPayloadSize = builder.udpPayloadSize;
        extendedRcode = 0;
        version = 0;
        int flags = 0;
        if (builder.dnssecOk) {
            flags |= FLAG_DNSSEC_OK;
        }
        dnssecOk = builder.dnssecOk;
        this.flags = flags;
        variablePart = Collections.emptyList();
    }

    public static Edns fromRecord(Record<? extends Data> record) {
        if (record.type != TYPE.OPT) return null;

        @SuppressWarnings("unchecked")
        Record<OPT> optRecord = (Record<OPT>) record;
        return new Edns(optRecord);
    }

    public static Builder builder() {
        return new Builder();
    }

    public Record<OPT> asRecord() {
        if (optRecord == null) {
            long optFlags = flags;
            optFlags |= (long) extendedRcode << 8;
            optFlags |= (long) version << 16;
            optRecord = new Record<>(DnsName.ROOT, Record.TYPE.OPT, udpPayloadSize, optFlags, new OPT(variablePart));
        }
        return optRecord;
    }

    public String asTerminalOutput() {
        if (terminalOutputCache == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("EDNS: version: ").append(version).append(", flags:");
            if (dnssecOk)
                sb.append(" do");
            sb.append("; udp: ").append(udpPayloadSize);
            if (!variablePart.isEmpty()) {
                sb.append('\n');
                Iterator<EdnsOption> it = variablePart.iterator();
                while (it.hasNext()) {
                    EdnsOption edns = it.next();
                    sb.append(edns.getOptionCode()).append(": ");
                    sb.append(edns.asTerminalOutput());
                    if (it.hasNext()) {
                        sb.append('\n');
                    }
                }
            }
            terminalOutputCache = sb.toString();
        }
        return terminalOutputCache;
    }

    @NonNull
    @Override
    public String toString() {
        return asTerminalOutput();
    }

    /**
     * The EDNS option code.
     *
     * @see <a href="http://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml#dns-parameters-11">IANA - DNS EDNS0 Option Codes (OPT)</a>
     */
    public enum OptionCode {
        UNKNOWN(-1),
        NSID(3),
        ;

        private static final Map<Integer, OptionCode> INVERSE_LUT = new HashMap<>(OptionCode.values().length);

        static {
            for (OptionCode optionCode : OptionCode.values()) {
                INVERSE_LUT.put(optionCode.asInt, optionCode);
            }
        }

        public final int asInt;

        OptionCode(int optionCode) {
            this.asInt = optionCode;
        }

        public static OptionCode from(int optionCode) {
            OptionCode res = INVERSE_LUT.get(optionCode);
            if (res == null) res = OptionCode.UNKNOWN;
            return res;
        }
    }

    public static final class Builder {
        private int udpPayloadSize;
        private boolean dnssecOk;


        private Builder() {
        }

        public Builder setUdpPayloadSize(int udpPayloadSize) {
            if (udpPayloadSize > 0xffff) {
                throw new IllegalArgumentException("UDP payload size must not be greater than 65536, was " + udpPayloadSize);
            }
            this.udpPayloadSize = udpPayloadSize;
            return this;
        }

        @SuppressWarnings("UnusedReturnValue")
        public Builder setDnssecOk(boolean dnssecOk) {
            this.dnssecOk = dnssecOk;
            return this;
        }

        public Edns build() {
            return new Edns(this);
        }
    }
}
