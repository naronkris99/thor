package threads.lite.server;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;

import bitswap.pb.MessageOuterClass;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.bitswap.BitSwapEngineHandler;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.core.Swarm;
import threads.lite.host.LiteHost;
import threads.lite.host.LitePullHandler;
import threads.lite.host.LitePushHandler;
import threads.lite.host.LiteResponder;
import threads.lite.ident.IdentityHandler;
import threads.lite.utils.MultistreamHandler;


public final class ServerSession implements Session {

    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public ServerSession(@NonNull BlockStore blockStore, @NonNull LiteHost host) {
        this.blockStore = blockStore;
        this.bitSwapEngine = new BitSwapEngine(blockStore);

        // add the default
        try {
            addProtocolHandler(new MultistreamHandler());
            addProtocolHandler(new LitePushHandler(host));
            addProtocolHandler(new LitePullHandler(host));
            addProtocolHandler(new IdentityHandler(host, this));
            addProtocolHandler(new BitSwapEngineHandler(bitSwapEngine));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception {

        if (isClosed()) throw new IllegalStateException("Session is closed");

        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new Exception("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new Exception("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new Exception("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        // important, protocols should note be modified "outside"
        return Collections.unmodifiableMap(protocols);
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        throw new Exception("should not be invoked here");
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        throw new Exception("should not be invoked here");
    }

    @Override
    public boolean isClosed() {
        return closed.get();
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Override
    public boolean isFindProvidersActive() {
        return false;
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new LiteResponder(protocols);
    }

    @Override
    @NonNull
    public Supplier<Swarm> getSwarm() {
        return Swarm::new;
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked here");
    }

    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return Collections.emptySet();
    }

    @Override
    public void close() {
        closed.set(true);
        bitSwapEngine.close();
    }


}

