package threads.lite.core;


import java.nio.ByteBuffer;

// Note: a protocol handler is only invoked, when a remote peer initiate a
// stream over an existing connection
public interface ProtocolHandler {

    // name of the protocol, starts with "/", e.g. "/libp2p/dcutr"
    String getProtocol();

    // is invoked, when the your protocol is requested
    void protocol(Stream stream) throws Exception;

    // is invoked, when data is arriving associated with your protocol
    void data(Stream stream, ByteBuffer data) throws Exception;
}
