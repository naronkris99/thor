package threads.lite.core;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import circuit.pb.Voucher;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.LiteHost;

public class Reservation {
    private static final String TAG = Reservation.class.getSimpleName();
    @NonNull
    private final Limit limit;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Connection connection;
    @NonNull
    private final Multiaddr relayAddress;
    @NonNull
    private final Circuit.Reservation reservation;

    public Reservation(@NonNull LiteHost host,
                       @NonNull Limit limit,
                       @NonNull Connection connection,
                       @NonNull Multiaddr relayAddress,
                       @NonNull Circuit.Reservation reservation) {
        this.host = host;
        this.limit = limit;
        this.connection = connection;
        this.relayAddress = relayAddress;
        this.reservation = reservation;
        if (LogUtils.isDebug()) {
            // currently no reason to get the voucher
            debug();
        }
    }


    // when 0, there is not limitation of bytes during communication
    public long getLimitData() {
        return limit.getData();
    }

    // when 0, there is no time limitation
    public long getLimitDuration() {
        return limit.getDuration();
    }

    @NonNull
    public Set<Multiaddr> getAddresses() {
        Set<Multiaddr> multiaddrs = new HashSet<>();

        for (ByteString raw : reservation.getAddrsList()) {
            try {
                Multiaddr multiaddr = Multiaddr.createCircuit(
                        host.self(), raw.asReadOnlyByteBuffer());

                if (multiaddr.protocolSupported(host.ipv(),
                        true)) {
                    multiaddrs.add(multiaddr);
                }
            } catch (Throwable ignore) {
            }
        }

        return multiaddrs;
    }


    @NonNull
    public Multiaddr getRelayAddress() {
        return relayAddress;
    }


    public long expireInMinutes() {
        Date expire = new Date(reservation.getExpire() * 1000);
        Date now = new Date();
        long duration = expire.getTime() - now.getTime();
        return TimeUnit.MILLISECONDS.toMinutes(duration);
    }


    @NonNull
    @Override
    public String toString() {
        return "Reservation{" +
                " limit=" + limit +
                ", relayAddress=" + relayAddress +
                '}';
    }

    private void debug() {

        if (reservation.hasVoucher()) {
            try {
                Voucher.ReservationVoucher voucher =
                        Voucher.ReservationVoucher.parseFrom(reservation.getVoucher());

                LogUtils.error(TAG, "hasExpiration " + voucher.hasExpiration());
                LogUtils.error(TAG, "Expiration " + voucher.getExpiration());

                LogUtils.error(TAG, new String(voucher.getPeer().toByteArray()));
                LogUtils.error(TAG, new String(voucher.getRelay().toByteArray()));

            } catch (Throwable throwable) {
                LogUtils.debug(TAG, "hasVoucher " + throwable.getMessage());
            }
        }

    }

    @NonNull
    public Kind getKind() {
        return limit.getKind();
    }

    @NonNull
    public PeerId getRelayId() {
        return getRelayAddress().getPeerId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return connection.equals(that.connection) &&
                relayAddress.equals(that.relayAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connection, relayAddress);
    }

    @NonNull
    public Connection getConnection() {
        return connection;
    }

    public boolean isStaticRelay() {
        return getKind() == Kind.STATIC;
    }

    public enum Kind {
        LIMITED, STATIC
    }

    public static class Limit {
        private final long data;
        private final long duration;
        private final Reservation.Kind kind;

        public Limit(long data, long duration, Kind kind) {
            this.data = data;
            this.duration = duration;
            this.kind = kind;
        }

        public long getData() {
            return data;
        }

        public long getDuration() {
            return duration;
        }

        public Kind getKind() {
            return kind;
        }

        @NonNull
        @Override
        public String toString() {
            return "Limit{" +
                    "data=" + data +
                    ", duration=" + duration +
                    ", kind=" + kind +
                    '}';
        }
    }
}
