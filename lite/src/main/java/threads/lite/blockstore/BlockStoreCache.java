package threads.lite.blockstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;
import java.util.UUID;

import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;


public class BlockStoreCache implements BlockStore, AutoCloseable {

    private static final String TAG = BlockStoreCache.class.getSimpleName();
    private final BlocksStoreDatabase blocksStoreDatabase;
    private final String name;
    private final Context context;

    private BlockStoreCache(Context context, BlocksStoreDatabase blocksStoreDatabase, String name) {
        this.context = context;
        this.blocksStoreDatabase = blocksStoreDatabase;
        this.name = name;
    }

    public static BlockStoreCache createInstance(@NonNull Context context) {

        UUID uuid = UUID.randomUUID();
        BlocksStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context,
                        BlocksStoreDatabase.class,
                        uuid.toString()).
                allowMainThreadQueries().
                fallbackToDestructiveMigration().build();
        return new BlockStoreCache(context, blocksStoreDatabase, uuid.toString());

    }


    @Override
    public void clear() {
        blocksStoreDatabase.clearAllTables();
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public Block getBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().getBlock(cid);
    }

    @Override
    public void storeBlock(@NonNull Block block) {
        blocksStoreDatabase.blockDao().insertBlock(block);
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        blocksStoreDatabase.blockDao().deleteBlock(cid);
    }

    @Override
    public void deleteBlocks(@NonNull List<Cid> cids) {
        for (Cid cid : cids) {
            deleteBlock(cid);
        }
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public void close() {
        clear();
        boolean success = context.deleteDatabase(getName());
        LogUtils.info(TAG, "Delete success " + success + " of database " + getName());

    }
}
