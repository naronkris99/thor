package threads.lite.peerstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Peer;
import threads.lite.core.PeerStore;


public class PEERS implements PeerStore {

    private static volatile PEERS INSTANCE = null;
    private final PeerStoreDatabase peerStoreDatabase;

    private PEERS(PeerStoreDatabase peerStoreDatabase) {
        this.peerStoreDatabase = peerStoreDatabase;
    }

    @NonNull
    private static PEERS createPeerStore(@NonNull PeerStoreDatabase peerStoreDatabase) {
        return new PEERS(peerStoreDatabase);
    }

    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    PeerStoreDatabase blocksDatabase = Room.databaseBuilder(context, PeerStoreDatabase.class,
                                    PeerStoreDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = PEERS.createPeerStore(blocksDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public List<Peer> getRandomPeers(int limit) {
        return peerStoreDatabase.peerStoreDao().getRandomPeers(limit);
    }

    @Override
    public void insertPeer(@NonNull Peer peer) {
        peerStoreDatabase.peerStoreDao().insertPeer(peer);
    }

}
