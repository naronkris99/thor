package threads.lite.cid;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;

import androidx.annotation.NonNull;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Network {

    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @NonNull
    public static InetSocketAddress getSiteLocalAddress(int port) throws Exception {
        List<InetSocketAddress> list = getSiteLocalAddresses(port);
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new Exception("no site local address");
    }

    @NonNull
    public static List<InetSocketAddress> getSiteLocalAddresses(int port) throws SocketException {
        List<InetSocketAddress> multiaddrs = new ArrayList<>();
        List<NetworkInterface> interfaces = Collections.list(
                NetworkInterface.getNetworkInterfaces());
        for (NetworkInterface networkInterface : interfaces) {
            if (networkInterface.isUp()) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (inetAddress instanceof Inet4Address) {
                        if (inetAddress.isSiteLocalAddress()) {
                            multiaddrs.add(new InetSocketAddress(inetAddress, port));
                        }
                    }
                }
            }
        }
        return multiaddrs;
    }

    public static List<InetAddress> networkAddresses() throws SocketException {
        List<InetAddress> inetAddresses = new ArrayList<>();

        List<NetworkInterface> interfaces = Collections.list(
                NetworkInterface.getNetworkInterfaces());
        for (NetworkInterface networkInterface : interfaces) {
            if (networkInterface.isUp()) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (!Multiaddr.isLocalAddress(inetAddress)) {
                        inetAddresses.add(inetAddress);
                    }
                }
            }
        }

        return inetAddresses;
    }
}
