package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import com.google.protobuf.ByteString;

import net.luminis.quic.Version;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import multiaddr.pb.MultiaddrOuterClass;
import threads.lite.LogUtils;
import threads.lite.utils.DataHandler;


public final class Multiaddr {
    private final List<Tag> parts;
    private final PeerId peerId;

    private Multiaddr(PeerId peerId, List<Tag> parts) {
        this.peerId = peerId;
        this.parts = parts;
    }


    @NonNull
    @TypeConverter
    public static Multiaddr fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        try {
            MultiaddrOuterClass.Multiaddr stored =
                    MultiaddrOuterClass.Multiaddr.parseFrom(data);
            PeerId peerId = PeerId.create(stored.getId().toByteArray());
            return Multiaddr.create(peerId, stored.getAddrs().asReadOnlyByteBuffer());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Multiaddr multiaddr) {
        if (multiaddr == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }
        return MultiaddrOuterClass.Multiaddr.newBuilder()
                .setId(ByteString.copyFrom(multiaddr.getPeerId().getBytes()))
                .setAddrs(ByteString.copyFrom(multiaddr.getBytes()))
                .build().toByteArray();

    }

    @NonNull
    public static Multiaddr getLocalHost(Version version, PeerId peerId, int port) throws Exception {
        return Multiaddr.create(version, peerId, new InetSocketAddress(
                InetAddress.getLocalHost(), port));
    }

    @NonNull
    public static Multiaddr getSiteLocalAddress(Version version, PeerId peerId, int port) throws Exception {
        InetSocketAddress inetSocketAddress = Network.getSiteLocalAddress(port);
        return create(version, peerId, inetSocketAddress);
    }

    @NonNull
    public static Multiaddr create(String address) throws Exception {
        PeerId peerId = Protocol.getPeerId(address);
        return create(peerId, address);
    }

    @NonNull
    private static Multiaddr validate(PeerId peerId, List<Tag> tags) {

        boolean isCircuit = false;
        Protocol p2p = Protocol.get(Type.P2P);
        Protocol circuit = Protocol.get(Type.P2PCIRCUIT);
        List<Tag> list = new ArrayList<>();
        for (int i = 0; i < tags.size(); i++) {
            Tag tag = tags.get(i);
            if (Objects.equals(tag, p2p)) {
                list.add(tags.get(i + 1));
            }
            if (Objects.equals(tag, circuit)) {
                isCircuit = true;
            }
        }
        if (list.size() > 1) {
            throw new RuntimeException("contains invalid num of peers");
        } else if (list.size() == 1) {
            if (!isCircuit) {
                throw new RuntimeException("invalid address");
            }
        }
        return new Multiaddr(peerId, tags);
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, String address) throws Exception {
        byte[] data = Protocol.decode(address);
        List<Tag> parts = parseMultiaddr(ByteBuffer.wrap(data));
        List<Tag> reduced = Protocol.reducePeerId(peerId, parts);
        return validate(peerId, reduced);
    }

    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, Multiaddr multiaddr) throws Exception {
        return create(multiaddr.toString()
                .concat("/")
                .concat(Type.P2PCIRCUIT)
                .concat("/")
                .concat(Type.P2P)
                .concat("/")
                .concat(peerId.toBase58()));
    }

    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, ByteBuffer raw) throws Exception {
        List<Tag> parts = parseMultiaddr(raw);
        parts.add(Protocol.get(Type.P2PCIRCUIT));
        return validate(peerId, parts);
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, ByteBuffer raw) throws Exception {
        List<Tag> parts = parseMultiaddr(raw);
        return validate(peerId, parts);
    }


    @NonNull
    private static List<Tag> parseMultiaddr(ByteBuffer in) throws Exception {
        List<Tag> parts = new ArrayList<>();

        while (in.hasRemaining()) {
            int code = DataHandler.readUnsignedVariant(in);
            Protocol p = Protocol.get(code);
            parts.add(p);
            if (p.size() == 0)
                continue;

            Tag tag = p.readTag(in);
            Objects.requireNonNull(tag);
            parts.add(tag);
        }
        return parts;
    }


    // circuit addresses are sorted out here
    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings,
                                    IPV ipv, boolean strictSupportAddress) {
        Multiaddrs result = new Multiaddrs();
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        for (Multiaddr addr : multiaddrs) {
            if (!addr.isCircuitAddress()) {
                if (addr.protocolSupported(ipv, strictSupportAddress)) {
                    result.add(addr);
                }
            }
        }
        return result;
    }

    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = new Multiaddrs();
        for (ByteString entry : byteStrings) {
            try {
                multiaddrs.add(Multiaddr.create(peerId, entry.asReadOnlyByteBuffer()));
            } catch (Throwable ignore) {
                // can happen of not supported multi addresses like tcp, etc.
            }
        }
        return multiaddrs;
    }


    @NonNull
    public static Multiaddr create(Version version, PeerId peerId, InetSocketAddress inetSocketAddress) {
        List<Tag> parts = Protocol.createAddress(inetSocketAddress, version);
        return validate(peerId, parts);
    }

    @NonNull
    public static Multiaddr createCircuit(Version version, PeerId peerId, PeerId relayId,
                                          InetSocketAddress inetSocketAddress) {
        List<Tag> parts = Protocol.createAddress(inetSocketAddress, version);
        parts.add(Protocol.get(Type.P2P));
        parts.add(relayId);
        parts.add(Protocol.get(Type.P2PCIRCUIT));
        return validate(peerId, parts);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean weakLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress());
    }

    @NonNull
    public static Multiaddr upgrade(Multiaddr multiaddr, InetAddress address) {
        Tag ip = Protocol.get(Type.IP4);
        if (address instanceof Inet6Address) {
            ip = Protocol.get(Type.IP6);
        }
        multiaddr.parts.set(0, ip);
        multiaddr.parts.set(1, new Tag.InetAddress(address.getAddress()));
        return multiaddr;
    }


    @NonNull
    public InetSocketAddress getInetSocketAddress() {
        return new InetSocketAddress(getHost(), getPort());
    }

    public boolean isAnyDns() {
        return isDnsaddr() || isDns() || isDns4() || isDns6();
    }

    public boolean strictSupportedAddress() {
        try {
            if (!isAnyDns()) {
                InetAddress inetAddress = getInetAddress();
                return !Multiaddr.isLocalAddress(inetAddress);
            } else {
                return true;
            }
        } catch (Throwable ignore) {
            LogUtils.error(getClass().getSimpleName(), toString());
            // nothing to do here
        }
        return false;
    }

    public boolean weakSupportedAddress() {
        try {
            if (isAnyDns()) {
                return true;
            } else {
                InetAddress inetAddress = getInetAddress();
                return !Multiaddr.weakLocalAddress(inetAddress);
            }
        } catch (Throwable ignore) {
            // nothing to do here
            LogUtils.error(getClass().getSimpleName(), toString());
        }
        return false;
    }

    public boolean hasQuic() {
        return has(Set.of(Protocol.get(Type.QUIC), Protocol.get(Type.QUICV1)));
    }

    public boolean protocolSupported(IPV ipv, boolean strictSupportAddress) {

        if (ipv == IPV.IPv4) {
            if (isIP6() || isDns6()) {
                return false;
            }
        }
        if (ipv == IPV.IPv6) {
            if (isIP4() || isDns4()) {
                return false;
            }
        }

        if (isDnsaddr()) {
            return true;
        }
        if (isDns()) {
            return hasQuic();
        }
        if (isDns4()) {
            return hasQuic();
        }
        if (isDns6()) {
            return hasQuic();
        }
        if (hasQuic()) {
            if (strictSupportAddress) {
                return strictSupportedAddress();
            } else {
                return weakSupportedAddress();
            }
        }
        return false;
    }

    public byte[] getBytes() {
        return Protocol.decode(parts);
    }


    @NonNull
    public String getHost() {
        return parts.get(1).toString();
    }

    @NonNull
    public InetAddress getInetAddress() throws UnknownHostException {
        Tag part = parts.get(1); // this is always the host address
        if (part instanceof Tag.Address) {
            throw new UnknownHostException("host must be resolved first");
        } else if (part instanceof Tag.InetAddress) {
            Tag.InetAddress inetAddress = (Tag.InetAddress) part;
            Objects.requireNonNull(inetAddress);
            return InetAddress.getByAddress(inetAddress.getAddress());
        } else {
            throw new UnknownHostException("unknown host");
        }
    }

    @NonNull
    private List<PeerId> getPeerIds() {
        List<PeerId> result = new ArrayList<>();
        for (int i = 0; i < parts.size(); i++) {
            Tag tag = parts.get(i);
            if (tag instanceof PeerId) {
                PeerId entry = (PeerId) tag;
                Objects.requireNonNull(entry);
                result.add(entry);
            }
        }
        return result;
    }

    public int getPort() {
        Tag part = parts.get(3);
        Objects.requireNonNull(part);
        Tag.Port port = (Tag.Port) part;
        Objects.requireNonNull(port);
        return port.getPort();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiaddr multiaddr = (Multiaddr) o;
        return Objects.equals(peerId, multiaddr.peerId) && parts.equals(multiaddr.parts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parts, peerId);
    }

    @NonNull
    @Override
    public String toString() {
        return Protocol.toAddress(parts) + "/" + Type.P2P + "/" + peerId.toString();
    }

    private boolean has(Set<Tag> types) {
        for (Tag part : parts) {
            if (types.contains(part)) {
                return true;
            }
        }
        return false;
    }


    public boolean isIP4() {
        return Objects.equals(parts.get(0).toString(), Type.IP4);
    }

    public boolean isIP6() {
        return Objects.equals(parts.get(0).toString(), Type.IP6);
    }

    public boolean isDns() {
        return Objects.equals(parts.get(0).toString(), Type.DNS);
    }

    public boolean isDns6() {
        return Objects.equals(parts.get(0).toString(), Type.DNS6);
    }

    public boolean isDns4() {
        return Objects.equals(parts.get(0).toString(), Type.DNS4);
    }

    public boolean isDnsaddr() {
        return Objects.equals(parts.get(0).toString(), Type.DNSADDR);
    }

    public boolean isCircuitAddress() {
        return has(Set.of(Protocol.get(Type.P2PCIRCUIT)));
    }

    public boolean isQuicV1() {
        return has(Set.of(Protocol.get(Type.QUICV1)));
    }

    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    @NonNull
    public PeerId getRelayId() throws Exception {
        if (!isCircuitAddress()) {
            throw new Exception("no circuit address");
        }
        List<PeerId> list = getPeerIds();
        if (list.size() != 1) {
            throw new RuntimeException("no valid circuit address");
        }
        return list.get(0);
    }

    public boolean isAnyLocalAddress() {
        try {
            InetAddress inetAddress = getInetAddress();
            return Multiaddr.isLocalAddress(inetAddress);
        } catch (Throwable ignore) {
            // nothing to do here
            LogUtils.error(getClass().getSimpleName(), toString());
        }
        return false;
    }

    @NonNull
    public Multiaddr getRelayAddress() throws Exception {
        if (!isCircuitAddress()) {
            throw new Exception("not a circuit address");
        }
        PeerId relayId = getRelayId();
        Objects.requireNonNull(relayId);
        InetSocketAddress inetSocketAddress = getInetSocketAddress();
        Objects.requireNonNull(inetSocketAddress);
        Version version = Version.IETF_draft_29;
        if (isQuicV1()) {
            version = Version.QUIC_version_1;
        }
        return create(version, relayId, inetSocketAddress);
    }
}
