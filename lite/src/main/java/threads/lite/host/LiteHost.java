package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.PeerId;
import threads.lite.core.AutonatResult;
import threads.lite.core.BlockStore;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Keys;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.core.Swarm;
import threads.lite.crypto.Key;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;
import threads.lite.relay.RelayConnection;
import threads.lite.relay.RelayService;
import threads.lite.server.ServerResponder;
import threads.lite.server.ServerSession;
import threads.lite.utils.PackageReader;


public final class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);

    @NonNull
    private final AtomicReference<IPV> version = new AtomicReference<>(IPV.IPv4v6);
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final Keys keys;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteCertificate liteCertificate;
    @NonNull
    private final AtomicReference<Multiaddr> dialableAddress = new AtomicReference<>();
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();

    @NonNull
    private final DnsClient dnsClient;

    @NonNull
    private final AtomicReference<LiteServer> liteServer = new AtomicReference<>();
    @Nullable
    private Consumer<LitePush> incomingPush;

    @Nullable
    private Supplier<IpnsRecord> recordSupplier;

    public LiteHost(@NonNull Keys keys, @NonNull BlockStore blockStore, @NonNull PeerStore peerStore)
            throws Exception {

        this.liteCertificate = LiteCertificate.createCertificate(keys);
        this.keys = keys;
        this.blockStore = blockStore;
        this.peerStore = peerStore;

        this.self = Key.createPeerId(keys.getPublic());
        evaluateProtocol();


        this.dnsClient = DnsResolver.getInstance(() -> {
            switch (version.get()) {
                case IPv4:
                    return new ArrayList<>(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                case IPv6:
                    return new ArrayList<>(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                default:
                    ArrayList<InetAddress> list = new ArrayList<>();
                    list.addAll(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                    list.addAll(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                    return list;
            }
        });


    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    public DnsClient getDnsClient() {
        return dnsClient;
    }

    public void setRecordSupplier(@Nullable Supplier<IpnsRecord> recordSupplier) {
        this.recordSupplier = recordSupplier;
    }

    @NonNull
    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }


    @NonNull
    public Keys getKeys() {
        return keys;
    }


    @NonNull
    public Set<Reservation> reservations() {
        return reservations;
    }


    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 @NonNull Supplier<Swarm> swarm,
                                 boolean findProvidersActive) {
        return new LiteSession(blockStore, this, swarm, findProvidersActive);
    }

    @NonNull
    public PeerId self() {
        return self;
    }


    @NonNull
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Nullable
    private Multiaddr resolveAddress(@NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
            return DnsResolver.resolveDns(getDnsClient(), multiaddr);
        } else if (multiaddr.isDnsaddr()) {
            List<Multiaddr> list = DnsResolver.resolveDnsaddr(getDnsClient(),
                    ipv(), multiaddr);
            if (!list.isEmpty()) {
                return list.get(0);
            }
        } else {
            return multiaddr;
        }
        return null;
    }


    @Nullable
    public Multiaddr resolveToOne(@NonNull Set<Multiaddr> multiaddrs) {
        List<Multiaddr> all = new ArrayList<>();
        for (Multiaddr multiaddr : multiaddrs) {
            Multiaddr resolved = resolveAddress(multiaddr);
            if (resolved != null) {
                all.add(resolved);
            }
        }
        return all.stream().findAny().orElse(null);
    }


    @NonNull
    public Set<Multiaddr> networkListenAddresses() {
        Set<Multiaddr> set = new HashSet<>();

        LiteServer server = liteServer.get();
        if (server != null) {
            int port = server.getPort();
            try {
                for (InetAddress inetAddress : Network.networkAddresses()) {
                    try {
                        // TODO should be activated when server is running
                    /*
                    set.add(Multiaddr.create(Version.QUIC_version_1,
                            self, new InetSocketAddress(inetAddress, port)));*/

                        set.add(Multiaddr.create(Version.IETF_draft_29,
                                self, new InetSocketAddress(inetAddress, port))); // todo should br removed as soon as possible
                    } catch (Throwable ignore) {
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return set;
    }


    @NonNull
    public Set<Multiaddr> listenAddresses() {
        Set<Multiaddr> set = new HashSet<>();
        Multiaddr dialable = dialableAddress.get();
        if (dialable != null) {
            set.add(dialable);
        } else {
            set.addAll(networkListenAddresses());
        }
        for (Reservation reservation : reservations()) {
            try {
                if (reservation.getConnection().isConnected()) {
                    set.addAll(reservation.getAddresses());
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return set;
    }


    @NonNull
    public Connection connect(Session session, Multiaddr address, Parameters parameters)
            throws ConnectException, InterruptedException {

        Multiaddr resolved = resolveAddress(address);
        if (resolved == null) {
            throw new ConnectException("no addresses left");
        }

        if (address.isCircuitAddress()) {
            try {

                RelayConnection relayConnection = RelayService.createRelayConnection(
                        this, session, address, parameters);

                return relayConnection.upgradeConnection();
            } catch (InterruptedException | ConnectException exception) {
                throw exception;
            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }
        } else {
            PeerId peerId = address.getPeerId();
            return connect(session.getStreamHandler(), peerId, address, parameters);

        }
    }


    @NonNull
    public LiteConnection connect(StreamHandler streamHandler, PeerId peerId, Multiaddr address,
                                  Parameters parameters)
            throws ConnectException, InterruptedException {

        long start = System.currentTimeMillis();
        boolean run = false;

        QuicClientConnection connection = getConnection(streamHandler, peerId, address, parameters);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.error(TAG, " Success connect " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }


    @NonNull
    private QuicClientConnection getConnection(StreamHandler streamHandler, PeerId peerId,
                                               Multiaddr address, Parameters parameters)
            throws ConnectException {

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .trustManager(new LiteTrust(peerId))
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .host(address.getHost())
                .port(address.getPort())
                .alpn(IPFS.ALPN)
                .transportParams(parameters);

        if (address.isQuicV1()) {
            builder = builder.version(Version.QUIC_version_1);
        } else {
            builder = builder.version(Version.IETF_draft_29);
        }

        if (address.isAnyLocalAddress()) {
            builder = builder.initialRtt(100);
        }


        return builder.build(quicStream -> new PackageReader(streamHandler));
    }


    public void push(@NonNull Connection connection, @NonNull IpnsEntity ipnsEntity) {
        try {
            if (incomingPush != null) {
                incomingPush.accept(new LitePush(connection, ipnsEntity));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<LitePush> incomingPush) {
        this.incomingPush = incomingPush;
    }


    @NonNull
    public IdentifyOuterClass.Identify createIdentity(@NonNull Set<String> protocols,
                                                      @NonNull Set<Multiaddr> multiaddrs,
                                                      @Nullable Multiaddr observed) {

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(Key.createCryptoKey(
                        keys.getPublic()).toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);


        if (!multiaddrs.isEmpty()) {
            for (Multiaddr addr : multiaddrs) {
                builder.addListenAddrs(ByteString.copyFrom(addr.getBytes()));
            }
        }
        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        if (observed != null) {
            builder.setObservedAddr(ByteString.copyFrom(observed.getBytes()));
        }

        return builder.build();
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = new ArrayList<>();
        try {
            addresses.addAll(Network.networkAddresses());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        if (!addresses.isEmpty()) {
            version.set(getProtocol(addresses));
        } else {
            version.set(IPV.IPv4);
        }
    }


    @NonNull
    private IPV getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return IPV.IPv6; // prefer ipv6
        } else if (ipv4) {
            return IPV.IPv4;
        } else if (ipv6) {
            return IPV.IPv6;
        } else {
            return IPV.IPv4v6;
        }
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(this, conn);
    }

    @NonNull
    public PeerInfo getIdentity() throws Exception {
        IdentifyOuterClass.Identify identity = createIdentity(
                Set.of(IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL,
                        IPFS.LITE_PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                        IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP),
                listenAddresses(), null);
        return IdentityService.getPeerInfo(this, identity);

    }


    @NonNull
    public AutonatResult autonat(@NonNull Server server) {
        autonat.lock();
        try {
            Autonat autonat = new Autonat();
            AutonatService.autonat(this, server, autonat, getBootstrap());

            Multiaddr winner = autonat.winner();
            if (winner != null) {
                dialableAddress.set(winner);
            }

            return new AutonatResult() {
                @NonNull
                @Override
                public String toString() {
                    return "Success " + success() +
                            " Address " + dialableAddress() +
                            " NatType " + getNatType();
                }

                @NonNull
                @Override
                public NatType getNatType() {
                    return autonat.getNatType();
                }

                @Nullable
                @Override
                public Multiaddr dialableAddress() {
                    return winner;
                }

                @Override
                public boolean success() {
                    return winner != null;
                }
            };
        } finally {
            autonat.unlock();
        }
    }


    @NonNull
    public Set<Reservation> reservations(@NonNull Server server,
                                         @NonNull Set<Multiaddr> multiaddrs,
                                         long timeout) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.isStaticRelay()) {
                    // still valid
                    relaysStillValid.add(reservation.getRelayId());
                } else {
                    if (reservation.expireInMinutes() < 2) {
                        try {
                            RelayService.reservation(this, server,
                                    reservation.getRelayAddress()).get();
                            relaysStillValid.add(reservation.getRelayId());
                        } catch (Throwable throwable) {
                            // note RelayService reservation throws exceptions
                            list.remove(reservation);
                        }
                    } else {
                        // still valid
                        relaysStillValid.add(reservation.getRelayId());
                    }
                }
            }

            if (!multiaddrs.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : multiaddrs) {
                    service.execute(() -> {
                        try {
                            PeerId peerId = address.getPeerId();
                            Objects.requireNonNull(peerId);

                            if (!relaysStillValid.contains(peerId)) {
                                reservation(server, address).get();
                            } // else case: nothing to do here, reservation is sill valid

                        } catch (Throwable ignore) {
                        }
                    });

                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    public CompletableFuture<Reservation> reservation(@NonNull Server server,
                                                      @NonNull Multiaddr multiaddr) {

        return RelayService.reservation(this, server, multiaddr)
                .whenComplete((reservation, throwable) -> {
                            if (throwable != null) {
                                LogUtils.info(TAG, throwable.getMessage());
                            } else {
                                reservations.add(reservation);
                            }
                        }
                );
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public Set<Multiaddr> getBootstrap() {
        return DnsResolver.resolveDnsaddrHost(getDnsClient(), ipv(), IPFS.LIB2P_DNS);
    }


    @NonNull
    public IPV ipv() {
        return version.get();
    }

    // creates a self signed record (used for ipns)
    public byte[] createSelfSignedRecord(byte[] data, long sequence) throws Exception {
        Date eol = Date.from(new Date().toInstant().plus(IPFS.RECORD_EOL));

        Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
        ipns.pb.Ipns.IpnsEntry record =
                IpnsService.create(keys.getPrivate(), data, sequence, eol, duration);

        record = IpnsService.embedPublicKey(keys.getPublic(), record);

        return record.toByteArray();
    }


    public void punching(DatagramSocket socket, Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.getInetAddress();
            int port = multiaddr.getPort();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            LogUtils.error(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(socket, multiaddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
            LogUtils.error(getClass().getSimpleName(), toString());
        }
    }

    private int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    public void holePunchConnect(@NonNull DatagramSocket socket, @NonNull Set<Multiaddr> multiaddrs) {
        try {
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(socket, multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public PeerStore getPeerStore() {
        return peerStore;
    }

    @NonNull
    public IpnsRecord getIpnsRecord() throws Exception {
        if (recordSupplier != null) {
            return recordSupplier.get();
        }
        return new IpnsRecord(Key.createIpnsKey(self()),
                createSelfSignedRecord(new byte[0], 0));
    }

    @NonNull
    public LiteServer createServer(DatagramSocket socket,
                                   @NonNull Consumer<Connection> connectConsumer,
                                   @NonNull Consumer<Connection> closedConsumer,
                                   @NonNull Function<PeerId, Boolean> isGated) {

        ServerSession serverSession = new ServerSession(blockStore, this);

        ServerConnector server = createServerConnector(socket, new ServerResponder(serverSession));

        server.setClosedConsumer(connection -> closedConsumer.accept(new LiteConnection(connection)));

        server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public void createConnection(String protocol, QuicConnection quicConnection) {

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());

                LiteConnection liteConnection = new LiteConnection(quicConnection);

                try {
                    X509Certificate cert = quicConnection.getRemoteCertificate();
                    Objects.requireNonNull(cert);
                    PeerId remotePeerId = LiteCertificate.extractPeerId(cert);
                    Objects.requireNonNull(remotePeerId);

                    if (isGated.apply(remotePeerId)) {
                        throw new Exception("Peer is gated " + remotePeerId);
                    }

                    // now the remote PeerId is available
                    quicConnection.setAttribute(Connection.REMOTE_PEER, remotePeerId);

                } catch (Throwable throwable) {
                    quicConnection.close();
                    LogUtils.error(TAG, throwable);
                    return;
                }

                try {
                    connectConsumer.accept(liteConnection);
                } catch (Throwable ignore) {
                }

            }
        });

        server.start();
        return new LiteServer(liteCertificate, socket, server, serverSession,
                connectConsumer, closedConsumer);
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket, StreamHandler streamHandler) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.IETF_draft_29); // todo should br removed as soon as possible
        supportedVersions.add(Version.QUIC_version_1);
        return new ServerConnector(socket, new LiteTrust(), liteCertificate.certificate(),
                liteCertificate.privateKey(), supportedVersions,
                quicStream -> new PackageReader(streamHandler),
                false, false);

    }


    public Server startSever(int port,
                             @NonNull Consumer<Connection> connectConsumer,
                             @NonNull Consumer<Connection> closedConsumer,
                             @NonNull Function<PeerId, Boolean> isGated) {


        LiteServer check = liteServer.get();
        if (check != null) {
            check.shutdown();
            liteServer.set(null);
        }
        DatagramSocket socket = getSocket(port);


        liteServer.set(createServer(socket, connectConsumer, closedConsumer, isGated));

        return liteServer.get();
    }

    @Nullable
    public Server getServer() {
        return liteServer.get();
    }
}


