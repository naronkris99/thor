package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.Version;
import net.luminis.quic.server.ServerConnector;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.Server;
import threads.lite.core.StreamHandler;
import threads.lite.server.ServerSession;
import threads.lite.utils.PackageReader;

public class LiteServer implements Server {
    private static final String TAG = LiteServer.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final ServerSession serverSession;
    @NonNull
    private final Consumer<Connection> connectConsumer;
    @NonNull
    private final Consumer<Connection> closedConsumer;
    @NonNull
    private final LiteCertificate liteCertificate;


    public LiteServer(@NonNull LiteCertificate liteCertificate,
                      @NonNull DatagramSocket socket,
                      @NonNull ServerConnector serverConnector,
                      @NonNull ServerSession serverSession,
                      @NonNull Consumer<Connection> connectConsumer,
                      @NonNull Consumer<Connection> closedConsumer) {
        this.liteCertificate = liteCertificate;
        this.socket = socket;
        this.serverConnector = serverConnector;
        this.serverSession = serverSession;
        this.connectConsumer = connectConsumer;
        this.closedConsumer = closedConsumer;
    }

    @NonNull
    @Override
    public Consumer<Connection> getClosedConsumer() {
        return closedConsumer;
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }

    @NonNull
    public ServerConnector getServerConnector() {
        return serverConnector;
    }

    @NonNull
    @Override
    public ServerSession getServerSession() {
        return serverSession;
    }

    @NonNull
    @Override
    public Consumer<Connection> getConnectConsumer() {
        return connectConsumer;
    }

    @Override
    public void shutdown() {
        serverConnector.shutdown();
    }

    @Override
    public int getPort() {
        return socket.getLocalPort();
    }

    @Override
    public int numServerConnections() {
        return serverConnector.numConnections();
    }


    @NonNull
    private QuicClientConnection getConnection(StreamHandler streamHandler, PeerId peerId,
                                               Multiaddr address, Parameters parameters)
            throws ConnectException {

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .trustManager(new LiteTrust(peerId))
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .host(address.getHost())
                .port(address.getPort())
                .alpn(IPFS.ALPN)
                .transportParams(parameters);

        builder = builder.serverConnector(serverConnector);

        if (address.isQuicV1()) {
            builder = builder.version(Version.QUIC_version_1);
        } else {
            builder = builder.version(Version.IETF_draft_29);
        }

        if (address.isAnyLocalAddress()) {
            builder = builder.initialRtt(100);
        }


        return builder.build(quicStream -> new PackageReader(streamHandler));
    }

    @NonNull
    public Connection connect(StreamHandler streamHandler, Multiaddr address,
                              Parameters parameters)
            throws ConnectException, InterruptedException {

        long start = System.currentTimeMillis();
        boolean run = false;
        PeerId peerId = address.getPeerId();
        QuicClientConnection connection = getConnection(streamHandler, peerId, address,
                parameters);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.error(TAG, " Success dial " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

}
