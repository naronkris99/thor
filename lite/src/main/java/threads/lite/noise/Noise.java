package threads.lite.noise;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import payload.pb.Payload;
import threads.lite.IPFS;
import threads.lite.cid.PeerId;
import threads.lite.core.Keys;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.noise.protocol.DHState;
import threads.lite.noise.protocol.HandshakeState;
import threads.lite.noise.protocol.NoiseUtility;


public class Noise {
    // payloadSigPrefix is prepended to our Noise static key before signing with
    // our libp2p identity key.
    public final static String NOISE_LIBP_2_P_STATIC_KEY = "noise-libp2p-static-key:";
    public final static String PROTOCOL_NAME = "Noise_XX_25519_ChaChaPoly_SHA256";
    private static final int ENCODING = 2;


    public static byte[] encrypt(@NonNull CipherState cipherState, byte[] plaintext) throws Exception {
        byte[] ciphertext = new byte[plaintext.length + cipherState.getMACLength()];
        int length = cipherState.encryptWithAd(null, plaintext, 0, ciphertext,
                0, plaintext.length);
        return Arrays.copyOf(ciphertext, length);
    }

    public static byte[] decrypt(@NonNull CipherState cipherState, byte[] ciphertext)
            throws Exception {
        int length = cipherState.decryptWithAd(null, ciphertext,
                0, ciphertext, ciphertext.length);
        return Arrays.copyOf(ciphertext, length);
    }

    @Nullable
    private static PayloadInfo getPayloadInfo(@NonNull HandshakeState handshakeState,
                                              byte[] payload) throws Exception {
        // verify the signature of the remote's noise static public key once
        // the remote public key has been provided by the XX protocol
        DHState derivedRemotePublicKey = handshakeState.getRemotePublicKey();
        if (derivedRemotePublicKey.hasPublicKey()) {
            return Noise.getPayloadInfo(derivedRemotePublicKey, payload);
        }
        return null;
    }

    public static byte[] readNoiseMessage(@NonNull HandshakeState handshakeState, byte[] message)
            throws Exception {
        byte[] payload = new byte[message.length];
        int payloadLength = handshakeState.readMessage(message, 0,
                message.length, payload);
        return Arrays.copyOf(payload, payloadLength);
    }

    public static byte[] getNoiseMessage(@NonNull HandshakeState handshakeState, byte[] message)
            throws Exception {

        int msgLength = message.length;

        byte[] bytes = new byte[
                msgLength + (2 * (handshakeState.getLocalKeyPair().getPublicKeyLength()
                        + 16))]; // 16 is MAC length

        int length = handshakeState.writeMessage(bytes, 0,
                message, 0, msgLength);
        return Arrays.copyOfRange(bytes, 0, length);
    }

    public static PayloadInfo getPayloadInfo(@NonNull DHState remotePublicKeyState,
                                             byte[] payload) throws Exception {

        Payload.NoiseHandshakePayload noiseMsg = Payload.NoiseHandshakePayload.parseFrom(payload);
        PubKey publicKey = Key.unmarshalPublicKey(noiseMsg.getIdentityKey().toByteArray());
        byte[] signatureFromMessage = noiseMsg.getIdentitySig().toByteArray();

        publicKey.verify(Noise.noiseSignaturePhrase(remotePublicKeyState), signatureFromMessage);
        boolean skipMultiplex = false;

        /* TODO activate again when supported by libp2p
        Payload.NoiseExtensions extension = noiseMsg.getExtensions();
        if (extension != null) {
            List<String> list = extension.getStreamMuxersList();
            if (!list.contains(IPFS.MPLEX_PROTOCOL)) {
                throw new Exception("Mplex protocol not supported, abort");
            }
            skipMultiplex = true;
        } */

        return new PayloadInfo(PeerId.fromPubKey(publicKey), skipMultiplex);
    }

    public static byte[] generateHandshakePayload(Keys keys, DHState localNoiseState) throws Exception {

        // the payload consists of the identity public key, and the signature of the noise static public key
        // the actual noise static public key is sent later as part of the XX handshake
        // get identity public key
        byte[] pkBytes = Key.createCryptoKey(keys.getPublic()).toByteArray();

        // get noise static public key signature
        byte[] signedPayload = Key.sign(keys.getPrivate(),
                Noise.noiseSignaturePhrase(localNoiseState));

        // currently only the mplex protocol is supported
        Payload.NoiseExtensions extension = Payload.NoiseExtensions.newBuilder()
                .addStreamMuxers(IPFS.MPLEX_PROTOCOL).build();

        return Payload.NoiseHandshakePayload.newBuilder()
                .setIdentityKey(ByteString.copyFrom(pkBytes))
                .setIdentitySig(ByteString.copyFrom(signedPayload))
                .setExtensions(extension)
                .build().toByteArray();
    }

    public static byte[] noiseSignaturePhrase(@NonNull DHState dhState) {
        byte[] key = Noise.NOISE_LIBP_2_P_STATIC_KEY.getBytes();
        byte[] phrase = new byte[key.length + dhState.getPublicKeyLength()];
        System.arraycopy(key, 0, phrase, 0, key.length);
        dhState.getPublicKey(phrase, key.length);
        return phrase;
    }

    public static byte[] encodeNoiseMessage(byte[] noise) {
        ByteBuffer bigEndian = getBigEndianShort(noise.length);
        ByteBuffer out = ByteBuffer.allocate(noise.length + ENCODING);
        out.put(bigEndian.array());
        out.put(noise);
        return out.array();
    }

    public static byte[] decodeNoiseMessage(byte[] noise) {
        return Arrays.copyOfRange(noise, ENCODING, noise.length);
    }

    public static ByteBuffer getBigEndianShort(int value) {
        return ByteBuffer.allocate(ENCODING).order(ByteOrder.BIG_ENDIAN).put((byte) (value >>> 8)).put((byte) value);
    }

    @NonNull
    public static NoiseState getInitiator(PeerId peerId, Keys keys) throws Exception {
        return getNoiseState(peerId, keys, HandshakeState.INITIATOR);
    }

    @NonNull
    public static NoiseState getResponder(PeerId peerId, Keys keys) throws Exception {
        return getNoiseState(peerId, keys, HandshakeState.RESPONDER);
    }

    @NonNull
    private static NoiseState getNoiseState(PeerId peerId, Keys keys, int role) throws Exception {
        DHState localNoiseState = NoiseUtility.createDH("25519");
        localNoiseState.generateKeyPair();


        HandshakeState handshakeState = new HandshakeState(Noise.PROTOCOL_NAME, role);
        handshakeState.getLocalKeyPair().copyFrom(localNoiseState);
        handshakeState.start();
        return new NoiseState(peerId, localNoiseState, handshakeState, keys);
    }


    public static class Response {
        @Nullable
        private final CipherStatePair cipherStatePair;
        @Nullable
        private final byte[] message;
        private final boolean skipMultiplex; // todo in the future it can be removed

        public Response(@Nullable CipherStatePair cipherStatePair, @Nullable byte[] message, boolean skipMultiplex) {
            this.cipherStatePair = cipherStatePair;
            this.message = message;
            this.skipMultiplex = skipMultiplex;
        }

        public boolean isSkipMultiplex() {
            return skipMultiplex;
        }

        @Nullable
        public CipherStatePair getCipherStatePair() {
            return cipherStatePair;
        }

        @Nullable
        public byte[] getMessage() {
            return message;
        }
    }

    public static class NoiseState {
        private final AtomicBoolean sentNoiseKeyPayload = new AtomicBoolean(false);
        private final HandshakeState handshakeState;
        private final DHState localNoiseState;
        private final Keys keys;
        private final PeerId peerId;

        public NoiseState(PeerId peerId, DHState localNoiseState,
                          HandshakeState handshakeState, Keys keys) {
            this.peerId = peerId;
            this.handshakeState = handshakeState;
            this.localNoiseState = localNoiseState;
            this.keys = keys;
        }

        public HandshakeState getHandshakeState() {
            return handshakeState;
        }

        public DHState getLocalNoiseState() {
            return localNoiseState;
        }


        public byte[] getInitalMessage() throws Exception {
            return Noise.getNoiseMessage(getHandshakeState(), new byte[0]);
        }


        @NonNull
        public Response handshake(byte[] msg) throws Exception {

            CipherStatePair cipherStatePair = null;
            byte[] message = null;
            boolean skipMultiplex = false;
            if (getHandshakeState().getAction() == HandshakeState.READ_MESSAGE) {
                byte[] payload = Noise.readNoiseMessage(getHandshakeState(), msg);

                // verify the signature of the remote's noise static public key once
                // the remote public key has been provided by the XX protocol
                PayloadInfo payloadInfo = getPayloadInfo(getHandshakeState(), payload);
                if (payloadInfo != null) { // todo check if it is never null
                    skipMultiplex = payloadInfo.isSkipMultiplex();
                    if (!Objects.equals(peerId, payloadInfo.getPeerId())) {
                        throw new Exception("InvalidRemotePubKey");
                    }
                }
            }


            // after reading messages and setting up state, write next message onto the wire
            if (getHandshakeState().getAction() == HandshakeState.WRITE_MESSAGE) {

                // only send the Noise static key once
                if (!sentNoiseKeyPayload.getAndSet(true)) {

                    // generate an appropriate protobuf element
                    byte[] noiseHandshakePayload =
                            Noise.generateHandshakePayload(keys, getLocalNoiseState());

                    // create the message with the signed payload -
                    // verification happens once the noise static key is shared

                    message = Noise.getNoiseMessage(getHandshakeState(), noiseHandshakePayload);
                }
            }

            if (getHandshakeState().getAction() == HandshakeState.SPLIT) {
                cipherStatePair = getHandshakeState().split();
            }
            return new Response(cipherStatePair, message, skipMultiplex);
        }
    }

    public static class PayloadInfo {
        private final PeerId peerId;
        private final boolean skipMultiplex; // todo in the future it can be removed

        public PayloadInfo(PeerId peerId, boolean skipMultiplex) {
            this.peerId = peerId;
            this.skipMultiplex = skipMultiplex;
        }

        public PeerId getPeerId() {
            return peerId;
        }

        public boolean isSkipMultiplex() {
            return skipMultiplex;
        }
    }
}
