package threads.lite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import net.luminis.quic.Version;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.blockstore.BlockStoreCache;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.Swarm;

public class IpfsDownloadTest {
    private static final String TAG = IpfsDownloadTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_client_server_download() throws Exception {

        Dummy dummy = Dummy.getInstance();

        IPFS ipfs = TestEnv.getTestInstance(context);

        Server server = ipfs.getServer();
        TestCase.assertNotNull(server);
        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB

        Cid cid;
        try (Session session = ipfs.createSession()) {
            AtomicInteger counter = new AtomicInteger(0);
            cid = ipfs.storeInputStream(session, new InputStream() {
                @Override
                public int read() {
                    int count = counter.incrementAndGet();
                    if (count > maxNumberBytes) {
                        return -1;
                    }
                    return 99;
                }
            });

        }
        assertNotNull(cid);
        long start = System.currentTimeMillis();

        PeerId host = ipfs.self();
        TestCase.assertNotNull(host);
        Multiaddr multiaddr = Multiaddr.getLocalHost(Version.QUIC_version_1,
                ipfs.self(), server.getPort());


        File file = TestEnv.createCacheFile(context);
        try (Swarm swarm = new Swarm()) {
            try (BlockStoreCache blockStore = BlockStoreCache.createInstance(context)) {
                try (Session dummySession = dummy.createSession(blockStore, () -> swarm)) {
                    Connection conn = dummy.getHost().connect(dummySession, multiaddr,
                            Parameters.getDefault());
                    Objects.requireNonNull(conn);
                    swarm.add(conn);

                    ipfs.fetchToFile(dummySession, file, cid, new Progress() {
                        @Override
                        public void setProgress(int progress) {
                            LogUtils.error(TAG, "Progress " + progress);
                        }

                        @Override
                        public boolean isCancelled() {
                            return false;
                        }
                    });
                }
            }
        }
        assertEquals(file.length(), maxNumberBytes);

        long end = System.currentTimeMillis();
        LogUtils.error(TAG, "Time for downloading " + (end - start) / 1000 +
                "[s]" + " " + file.length() / 1000000 + " [MB]");
        file.deleteOnExit();
    }

}
