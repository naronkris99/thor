package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.Version;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.ConnectException;
import java.nio.ByteBuffer;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteCertificate;
import threads.lite.utils.PackageReader;


@RunWith(AndroidJUnit4.class)
public class IpfsConnectTest {
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        LiteCertificate liteCertificate = LiteCertificate.createCertificate(ipfs.getKeys());

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .version(Version.IETF_draft_29)
                .trustManager(new DummyTrust())
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .host("147.28.156.11")
                .port(4001)
                .alpn(IPFS.ALPN)
                .transportParams(ipfs.getConnectionParameters());

        QuicClientConnectionImpl conn = builder.build(quicStream -> new PackageReader(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                LogUtils.warning(TAG, throwable.getMessage());
            }

            @Override
            public void protocol(Stream stream, String protocol) {
                LogUtils.warning(TAG, protocol);
            }

            @Override
            public void data(Stream stream, ByteBuffer data) {

            }
        }));
        conn.connect(5);


    }


    @Test(expected = ConnectException.class)
    public void swarm_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String address = "/ip4/139.178.68.146/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR";

            Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
            assertNotNull(multiaddr.getInetSocketAddress());
            assertEquals(multiaddr.getInetSocketAddress().getPort(), multiaddr.getPort());
            assertEquals(multiaddr.getInetSocketAddress().getHostName(), multiaddr.getHost());

            assertEquals(multiaddr.getPeerId(),
                    ipfs.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR"));


            // multiaddress is just a fiction
            Connection conn = ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());
            assertNull(conn); // does not come to this point

            fail(); // exception is thrown
        }
    }

}
