/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.server;

import static net.luminis.quic.QuicConnectionImpl.Status.Connected;
import static net.luminis.quic.QuicConstants.TransportErrorCode.INVALID_TOKEN;
import static net.luminis.quic.QuicConstants.TransportErrorCode.TRANSPORT_PARAMETER_ERROR;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.quic.CryptoStream;
import net.luminis.quic.DecryptionException;
import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.FrameProcessor2;
import net.luminis.quic.GlobalAckGenerator;
import net.luminis.quic.HandshakeState;
import net.luminis.quic.InvalidPacketException;
import net.luminis.quic.MissingKeysException;
import net.luminis.quic.PnSpace;
import net.luminis.quic.QuicConnectionImpl;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Role;
import net.luminis.quic.Settings;
import net.luminis.quic.TransportError;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.quic.cid.ConnectionIdManager;
import net.luminis.quic.frame.AckFrame;
import net.luminis.quic.frame.HandshakeDoneFrame;
import net.luminis.quic.frame.NewConnectionIdFrame;
import net.luminis.quic.frame.NewTokenFrame;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.frame.RetireConnectionIdFrame;
import net.luminis.quic.packet.HandshakePacket;
import net.luminis.quic.packet.InitialPacket;
import net.luminis.quic.packet.QuicPacket;
import net.luminis.quic.packet.RetryPacket;
import net.luminis.quic.packet.ShortHeaderPacket;
import net.luminis.quic.packet.VersionNegotiationPacket;
import net.luminis.quic.packet.ZeroRttPacket;
import net.luminis.quic.send.SenderImpl;
import net.luminis.quic.stream.StreamManager;
import net.luminis.quic.tls.QuicTransportParametersExtension;
import net.luminis.tls.NewSessionTicket;
import net.luminis.tls.TlsProtocolException;
import net.luminis.tls.alert.MissingExtensionAlert;
import net.luminis.tls.alert.NoApplicationProtocolAlert;
import net.luminis.tls.extension.ApplicationLayerProtocolNegotiationExtension;
import net.luminis.tls.extension.Extension;
import net.luminis.tls.handshake.CertificateMessage;
import net.luminis.tls.handshake.CertificateRequestMessage;
import net.luminis.tls.handshake.CertificateVerifyMessage;
import net.luminis.tls.handshake.EncryptedExtensions;
import net.luminis.tls.handshake.FinishedMessage;
import net.luminis.tls.handshake.NewSessionTicketMessage;
import net.luminis.tls.handshake.ServerHello;
import net.luminis.tls.handshake.ServerMessageSender;
import net.luminis.tls.handshake.TlsEngine;
import net.luminis.tls.handshake.TlsServerEngine;
import net.luminis.tls.handshake.TlsServerEngineFactory;
import net.luminis.tls.handshake.TlsStatusEventHandler;
import net.luminis.tls.util.ByteUtils;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;


public class ServerConnectionImpl extends QuicConnectionImpl implements ServerConnection,
        ServerConnectionProxy, TlsStatusEventHandler {
    private static final String TAG = ServerConnectionImpl.class.getSimpleName();
    private static final int TOKEN_SIZE = 37;
    private final SenderImpl sender;
    private final InetSocketAddress initialClientAddress;
    private final boolean retryRequired;
    private final GlobalAckGenerator ackGenerator;
    private final List<FrameProcessor2<AckFrame>> ackProcessors = new CopyOnWriteArrayList<>();
    private final TlsServerEngine tlsEngine;
    private final ApplicationProtocolRegistry applicationProtocolRegistry;
    private final Consumer<ServerConnectionImpl> closeCallback;
    private final StreamManager streamManager;

    private final int initialMaxStreamData;
    private final int maxOpenStreamsUni;
    private final int maxOpenStreamsBidi;
    private final byte[] token;
    private final ConnectionIdManager connectionIdManager;
    private final int maxIdleTimeoutInSeconds;
    private final int allowedClientConnectionIds = 3;
    private final AtomicLong bytesReceived = new AtomicLong(0L);
    private final AtomicReference<String> negotiatedApplicationProtocol = new AtomicReference<>();
    private final AtomicBoolean addressValidated = new AtomicBoolean(false);
    private final AtomicBoolean acceptedEarlyData = new AtomicBoolean(false);


    /**
     * Creates a server connection implementation.
     *
     * @param version                     quic version used for this connection
     * @param serverSocket                the socket that is used for sending packets
     * @param initialClientAddress        the initial client address (after handshake, clients can move to different address)
     * @param peerCid                     the connection id of the client
     * @param originalDcid                the original destination connection id used by the client
     * @param connectionIdLength          length of the connection id's generated and used by this connection (used as its source)
     * @param tlsServerEngineFactory      factory for creating tls engine
     * @param retryRequired               whether or not a retry is required for address validation
     * @param applicationProtocolRegistry the registry for application protocols this server supports
     * @param initialRtt                  the initial rtt
     * @param closeCallback               callback for notifying interested parties this connection is closed
     */
    protected ServerConnectionImpl(Version version, X509TrustManager trustManager,
                                   DatagramSocket serverSocket, InetSocketAddress initialClientAddress,
                                   byte[] peerCid, byte[] originalDcid, int connectionIdLength, TlsServerEngineFactory tlsServerEngineFactory,
                                   boolean retryRequired, ApplicationProtocolRegistry applicationProtocolRegistry,
                                   Integer initialRtt, ServerConnectionRegistry connectionRegistry,
                                   Consumer<ServerConnectionImpl> closeCallback,
                                   Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) {
        super(version, Role.Server);
        this.initialClientAddress = initialClientAddress;
        this.retryRequired = retryRequired;
        this.applicationProtocolRegistry = applicationProtocolRegistry;
        this.closeCallback = closeCallback;

        this.tlsEngine = tlsServerEngineFactory.createServerEngine(new TlsMessageSender(), this);
        this.tlsEngine.setTrustManager(trustManager);
        this.connectionSecrets.computeInitialKeys(originalDcid);

        sender = new SenderImpl(version, connectionSecrets, getIdleTimer(), this,
                getMaxPacketSize(), serverSocket, initialClientAddress, initialRtt, this::abortConnection);
        if (!retryRequired) {
            sender.setAntiAmplificationLimit(0);
        }

        BiConsumer<Integer, String> closeWithErrorFunction = (error, reason) ->
                immediateCloseWithError(EncryptionLevel.App, error, reason, false);
        connectionIdManager = new ConnectionIdManager(peerCid, originalDcid, connectionIdLength,
                allowedClientConnectionIds, connectionRegistry, sender, closeWithErrorFunction);

        ackGenerator = sender.getGlobalAckGenerator();
        registerProcessor(ackGenerator);

        Random random;
        if (retryRequired) {
            random = new SecureRandom();
            token = new byte[TOKEN_SIZE];
            random.nextBytes(token);
        } else {
            token = null;
        }

        sender.start();

        maxIdleTimeoutInSeconds = 30;
        initialMaxStreamData = Settings.INITIAL_MAX_STREAM_DATA_SERVER;
        maxOpenStreamsUni = Settings.MAX_STREAMS;
        maxOpenStreamsBidi = Settings.MAX_STREAMS;
        streamManager = new StreamManager(this, version, sender, getFlowController(),
                Role.Server, maxOpenStreamsUni, maxOpenStreamsBidi, streamDataConsumer);

    }

    public void abortConnection(Throwable error) {
        LogUtils.error(TAG, this + " aborted due to internal error", error);
        closeCallback.accept(this);
    }

    @Override
    protected SenderImpl getSender() {
        return sender;
    }

    @Override
    protected TlsEngine getTlsEngine() {
        return tlsEngine;
    }

    @Override
    protected GlobalAckGenerator getAckGenerator() {
        return ackGenerator;
    }

    @Override
    protected StreamManager getStreamManager() {
        return streamManager;
    }

    @Override
    public long getInitialMaxStreamData() {
        return initialMaxStreamData;
    }

    @Override
    public int getMaxShortHeaderPacketOverhead() {
        return 1  // flag byte
                + connectionIdManager.getCurrentPeerConnectionId().length
                + 4  // max packet number size, in practice this will be mostly 1
                + 16; // encryption overhead
    }

    @Override
    protected int getSourceConnectionIdLength() {
        return connectionIdManager.getInitialConnectionId().length;
    }

    public byte[] getInitialConnectionId() {
        return connectionIdManager.getInitialConnectionId();
    }

    @Override
    public byte[] getSourceConnectionId() {
        return connectionIdManager.getInitialConnectionId();
    }

    @Override
    public byte[] getDestinationConnectionId() {
        return connectionIdManager.getCurrentPeerConnectionId();
    }

    @Override
    public void registerProcessor(FrameProcessor2<AckFrame> ackProcessor) {
        ackProcessors.add(ackProcessor);
    }

    @Override
    public void earlySecretsKnown() {
        connectionSecrets.computeEarlySecrets(tlsEngine, quicVersion.getVersion());
    }

    @Override
    public void handshakeSecretsKnown() {
        connectionSecrets.computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
    }

    @Override
    public void handshakeFinished() {
        connectionSecrets.computeApplicationSecrets(tlsEngine);
        sender.enableAppLevel();
        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
        // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.1.2
        // "the TLS handshake is considered confirmed at the server when the handshake completes"
        getSender().discard(PnSpace.Handshake, "tls handshake confirmed");
        // TODO: discard keys too
        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
        // "The server MUST send a HANDSHAKE_DONE frame as soon as it completes the handshake."
        sendHandshakeDone(new HandshakeDoneFrame(quicVersion.getVersion()));
        connectionState.set(Connected);


        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                return HandshakeState.Confirmed;
            }
            return handshakeState;
        });
        if (state == HandshakeState.Confirmed) {
            handshakeStateListeners.forEach(l -> l.handshakeStateChangedEvent(
                    HandshakeState.Confirmed));
        } else {
            LogUtils.error(TAG, "Handshake server state cannot be set to Confirmed");
        }

        if (!acceptedEarlyData.get()) {
            applicationProtocolRegistry.startApplicationProtocolConnection(
                    negotiatedApplicationProtocol.get(), this);
        }
        connectionIdManager.handshakeFinished();
    }

    private void sendHandshakeDone(QuicFrame frame) {
        send(frame, this::sendHandshakeDone);
    }

    @Override
    public void newSessionTicketReceived(NewSessionTicket ticket) {
    }

    @Override
    public void extensionsReceived(List<Extension> extensions) throws TlsProtocolException {
        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.1
        // "Unless another mechanism is used for agreeing on an application protocol, endpoints MUST use ALPN for this purpose."
        Optional<Extension> alpnExtension = extensions.stream()
                .filter(ext -> ext instanceof ApplicationLayerProtocolNegotiationExtension)
                .findFirst();
        //noinspection SimplifyOptionalCallChains
        if (!alpnExtension.isPresent()) {
            throw new MissingExtensionAlert("missing application layer protocol negotiation extension");
        } else {
            // "When using ALPN, endpoints MUST immediately close a connection (...) if an application protocol is not negotiated."
            List<String> requestedProtocols = ((ApplicationLayerProtocolNegotiationExtension) alpnExtension.get()).getProtocols();
            Optional<String> applicationProtocol = applicationProtocolRegistry.selectSupportedApplicationProtocol(requestedProtocols);
            applicationProtocol
                    .map(protocol -> {
                        // Add negotiated protocol to TLS response (Encrypted Extensions message)
                        tlsEngine.addServerExtensions(new ApplicationLayerProtocolNegotiationExtension(protocol));
                        return protocol;
                    })
                    .map(selectedProtocol -> negotiatedApplicationProtocol.updateAndGet(s -> selectedProtocol))
                    .orElseThrow(() -> new NoApplicationProtocolAlert(requestedProtocols));
        }

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.2
        // "endpoints that receive ClientHello or EncryptedExtensions messages without the quic_transport_parameters extension
        //  MUST close the connection with an error of type 0x16d (equivalent to a fatal TLS missing_extension alert"
        Optional<Extension> tpExtension = extensions.stream()
                .filter(ext -> ext instanceof QuicTransportParametersExtension)
                .findFirst();
        //noinspection SimplifyOptionalCallChains
        if (!tpExtension.isPresent()) {
            throw new MissingExtensionAlert("missing quic transport parameters extension");
        } else {
            try {
                validateAndProcess(((QuicTransportParametersExtension) tpExtension.get()).getTransportParameters());
            } catch (TransportError transportParameterError) {
                throw new TlsProtocolException("transport parameter error", transportParameterError);
            }
        }

        TransportParameters serverTransportParams = new TransportParameters(maxIdleTimeoutInSeconds,
                initialMaxStreamData, maxOpenStreamsBidi, maxOpenStreamsUni);
        serverTransportParams.setActiveConnectionIdLimit(allowedClientConnectionIds);
        serverTransportParams.setDisableMigration(true);
        serverTransportParams.setInitialSourceConnectionId(connectionIdManager.getInitialConnectionId());
        serverTransportParams.setOriginalDestinationConnectionId(connectionIdManager.getOriginalDestinationConnectionId());
        if (retryRequired) {
            serverTransportParams.setRetrySourceConnectionId(connectionIdManager.getInitialConnectionId());
        }
        tlsEngine.setSelectedApplicationLayerProtocol(negotiatedApplicationProtocol.get());
        tlsEngine.addServerExtensions(new QuicTransportParametersExtension(quicVersion.getVersion(), serverTransportParams, Role.Server));
        tlsEngine.setSessionData(quicVersion.getVersion().getBytes());
        tlsEngine.setSessionDataVerificationCallback(this::acceptSessionResumption);
    }

    boolean acceptSessionResumption(ByteBuffer storedSessionData) {
        // https://www.ietf.org/archive/id/draft-ietf-quic-v2-05.html#name-tls-resumption-and-new_toke
        // "Servers MUST validate the originating version of any session ticket or token and not accept one issued from a different version."
        if (quicVersion.getVersion().equals(Version.parse(storedSessionData.getInt()))) {
            return true;
        } else {
            LogUtils.warning(TAG, "Resumed session denied because quic versions don't match");
            return false;
        }
    }

    @Override
    public boolean isEarlyDataAccepted() {

        // Remember that server connection actually accepted early data
        acceptedEarlyData.set(true);
        applicationProtocolRegistry.startApplicationProtocolConnection(
                negotiatedApplicationProtocol.get(), this);

        LogUtils.info(TAG, "Server accepted early data");
        return true;

    }

    @Override
    protected QuicPacket parsePacket(ByteBuffer data) throws MissingKeysException, DecryptionException, InvalidPacketException {
        try {
            return super.parsePacket(data);
        } catch (DecryptionException decryptionException) {
            if (retryRequired && (data.get(0) & 0b1111_0000) == 0b1100_0000) {
                // If retry packet has been sent, but lost, client will send another initial with keys based on odcid
                try {
                    data.rewind();
                    connectionSecrets.computeInitialKeys(connectionIdManager.getOriginalDestinationConnectionId());
                    return super.parsePacket(data);
                } finally {
                    connectionSecrets.computeInitialKeys(connectionIdManager.getInitialConnectionId());
                }
            } else {
                throw decryptionException;
            }
        }
    }

    @Override
    public void parseAndProcessPackets(Instant timeReceived, ByteBuffer data, QuicPacket parsedPacket) {
        if (InitialPacket.isInitial(data) && data.limit() < 1200) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14.1
            // "A server MUST discard an Initial packet that is carried in a UDP datagram with a payload that is smaller
            //  than the smallest allowed maximum datagram size of 1200 bytes."
            return;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8
        // "Therefore, after receiving packets from an address that is not yet validated, an endpoint MUST limit the
        //  amount of data it sends to the unvalidated address to three times the amount of data received from that address."
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
        // "For the purposes of avoiding amplification prior to address validation, servers MUST count all of the
        //  payload bytes received in datagrams that are uniquely attributed to a single connection. This includes
        //  datagrams that contain packets that are successfully processed and datagrams that contain packets that
        //  are all discarded."
        bytesReceived.getAndAdd(data.remaining());
        if (!addressValidated.get()) {
            sender.setAntiAmplificationLimit(3 * (int) bytesReceived.get());
        }

        super.parseAndProcessPackets(timeReceived, data, parsedPacket);
    }

    @Override
    public ProcessResult process(InitialPacket packet, Instant time) {

        if (retryRequired) {
            if (packet.getToken() == null) {
                sendRetry();
                connectionSecrets.computeInitialKeys(connectionIdManager.getInitialConnectionId());
                return ProcessResult.Abort;  // No further packet processing (e.g. ack generation).
            } else if (!Arrays.equals(packet.getToken(), token)) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-33#section-8.1.2
                // "If a server receives a client Initial that can be unprotected but contains an invalid Retry token,
                // (...), the server SHOULD immediately close (Section 10.2) the connection with an INVALID_TOKEN error."
                immediateCloseWithError(EncryptionLevel.Initial, INVALID_TOKEN.value,
                        null, false);
                return ProcessResult.Abort;
            } else {
                // Receiving a valid token implies address is validated.
                addressValidated.set(true);
                sender.unsetAntiAmplificationLimit();
                // Valid token, proceed as usual.
                processFrames(packet, time);
                return ProcessResult.Continue;
            }
        } else {
            processFrames(packet, time);
            return ProcessResult.Continue;
        }
    }

    private void sendRetry() {
        RetryPacket retry = new RetryPacket(quicVersion.getVersion(), connectionIdManager.getInitialConnectionId(),
                getDestinationConnectionId(), getOriginalDestinationConnectionId(), token);
        sender.send(retry);
    }

    @Override
    public ProcessResult process(ShortHeaderPacket packet, Instant time) {
        connectionIdManager.registerConnectionIdInUse(packet.getDestinationConnectionId());
        processFrames(packet, time);
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(VersionNegotiationPacket packet, Instant time) {
        // Intentionally discarding packet without any action (clients should not send Version Negotiation packets).
        return ProcessResult.Abort;
    }

    @Override
    public ProcessResult process(HandshakePacket packet, Instant time) {
        if (!addressValidated.getAndSet(true)) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-8.1
            // "In particular, receipt of a packet protected with Handshake keys confirms that the peer successfully processed
            //  an Initial packet. Once an endpoint has successfully processed a Handshake packet from the peer, it can consider
            //  the peer address to have been validated."
            sender.unsetAntiAmplificationLimit();
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2.2.1
        // "A server stops sending and processing Initial packets when it receives its first Handshake packet. "
        sender.discard(PnSpace.Initial, "first handshake packet received");  // Only discards when not yet done.
        processFrames(packet, time);
        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
        // "a server MUST discard Initial keys when it first successfully processes a Handshake packet"
        // TODO: discard keys too

        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(RetryPacket packet, Instant time) {
        // Intentionally discarding packet without any action (clients should not send Retry packets).
        return ProcessResult.Abort;
    }

    @Override
    public ProcessResult process(ZeroRttPacket packet, Instant time) {
        if (acceptedEarlyData.get()) {
            processFrames(packet, time);
        } else {
            LogUtils.warning(TAG, "Ignoring 0-RTT packet because server " +
                    "connection does not accept early data.");
        }
        return ProcessResult.Continue;
    }

    @Override
    public void process(AckFrame ackFrame, QuicPacket packet, Instant timeReceived) {
        ackProcessors.forEach(processor -> processor.process(ackFrame, packet.getPnSpace(), timeReceived));
    }

    @Override
    public void process(HandshakeDoneFrame handshakeDoneFrame, QuicPacket packet, Instant timeReceived) {
    }

    @Override
    public void process(NewConnectionIdFrame newConnectionIdFrame, QuicPacket packet, Instant timeReceived) {
        connectionIdManager.process(newConnectionIdFrame);
    }

    @Override
    public void process(NewTokenFrame newTokenFrame, QuicPacket packet, Instant timeReceived) {
    }

    @Override
    public void process(RetireConnectionIdFrame retireConnectionIdFrame, QuicPacket packet, Instant timeReceived) {
        connectionIdManager.process(retireConnectionIdFrame, packet.getDestinationConnectionId());
    }

    @Override
    public void terminate() {
        super.terminate();
        closeCallback.accept(this);
    }

    private void validateAndProcess(TransportParameters transportParameters) throws TransportError {
        if (transportParameters.getInitialMaxStreamsBidi() > 0x1000000000000000L) {
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }
        if (transportParameters.getMaxUdpPayloadSize() < 1200) {
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }
        if (transportParameters.getAckDelayExponent() > 20) {
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }
        if (transportParameters.getMaxAckDelay() > 16384) {
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }
        if (transportParameters.getActiveConnectionIdLimit() < 2) {
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }
        if (!connectionIdManager.validateInitialPeerConnectionId(transportParameters.getInitialSourceConnectionId())) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.3
            // "An endpoint MUST treat absence of the initial_source_connection_id transport parameter from either
            //  endpoint (...) as a connection error of type TRANSPORT_PARAMETER_ERROR."
            // "An endpoint MUST treat the following as a connection error of type TRANSPORT_PARAMETER_ERROR or
            //  PROTOCOL_VIOLATION: a mismatch between values received from a peer in these transport parameters and the
            //  value sent in the corresponding Destination or Source Connection ID fields of Initial packets."
            throw new TransportError(TRANSPORT_PARAMETER_ERROR);
        }

        determineIdleTimeout(maxIdleTimeoutInSeconds * 1000L, transportParameters.getMaxIdleTimeout());

        connectionIdManager.registerPeerCidLimit(transportParameters.getActiveConnectionIdLimit());

        getFlowController().init(transportParameters.getInitialMaxData(),
                transportParameters.getInitialMaxStreamDataBidiLocal(),
                transportParameters.getInitialMaxStreamDataBidiRemote(),
                transportParameters.getInitialMaxStreamDataUni());

        streamManager.setInitialMaxStreamsBidi(transportParameters.getInitialMaxStreamsBidi());
        streamManager.setInitialMaxStreamsUni(transportParameters.getInitialMaxStreamsUni());
    }

    @Override
    public InetAddress getInitialClientAddress() {
        return initialClientAddress.getAddress();
    }

    public boolean isClosed() {
        return connectionState.get().isClosed();
    }

    public byte[] getOriginalDestinationConnectionId() {
        return connectionIdManager.getOriginalDestinationConnectionId();
    }

    @Override
    public void parsePackets(Instant timeReceived, ByteBuffer data) {
        parseAndProcessPackets(timeReceived, data, null);
    }

    public List<byte[]> getActiveConnectionIds() {
        return connectionIdManager.getActiveConnectionIds();
    }

    @Override
    public void setMaxAllowedBidirectionalStreams(int max) {
    }

    @Override
    public void setMaxAllowedUnidirectionalStreams(int max) {
    }

    @Override
    public void setDefaultStreamReceiveBufferSize(long size) {
    }


    @Override
    public InetSocketAddress getRemoteAddress() {
        return initialClientAddress;
    }

    @NonNull
    @Override
    public String toString() {
        return "ServerConnection[" + ByteUtils.bytesToHex(connectionIdManager.getOriginalDestinationConnectionId()) + "]";
    }

    private class TlsMessageSender implements ServerMessageSender {
        @Override
        public void send(ServerHello sh) {
            CryptoStream cryptoStream = getCryptoStream(EncryptionLevel.Initial);
            cryptoStream.write(sh, false);
        }

        @Override
        public void send(EncryptedExtensions ee) {
            getCryptoStream(EncryptionLevel.Handshake).write(ee, false);
        }

        @Override
        public void send(CertificateRequestMessage cm) throws IOException {
            getCryptoStream(EncryptionLevel.Handshake).write(cm, false);
        }

        @Override
        public void send(CertificateMessage cm) throws IOException {
            getCryptoStream(EncryptionLevel.Handshake).write(cm, false);
        }

        @Override
        public void send(CertificateVerifyMessage cv) throws IOException {
            getCryptoStream(EncryptionLevel.Handshake).write(cv, false);
        }

        @Override
        public void send(FinishedMessage finished) throws IOException {
            CryptoStream cryptoStream = getCryptoStream(EncryptionLevel.Handshake);
            cryptoStream.write(finished, false);
        }

        @Override
        public void send(NewSessionTicketMessage newSessionTicket) throws IOException {
            CryptoStream cryptoStream = getCryptoStream(EncryptionLevel.App);
            cryptoStream.write(newSessionTicket, true);
        }
    }
}
