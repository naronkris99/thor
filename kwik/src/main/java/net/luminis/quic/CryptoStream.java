/*
 * Copyright © 2018, 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.quic.frame.CryptoFrame;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.send.Sender;
import net.luminis.quic.tls.QuicTransportParametersExtension;
import net.luminis.tls.HandshakeType;
import net.luminis.tls.Message;
import net.luminis.tls.ProtectionKeysType;
import net.luminis.tls.TlsProtocolException;
import net.luminis.tls.extension.Extension;
import net.luminis.tls.handshake.HandshakeMessage;
import net.luminis.tls.handshake.TlsEngine;
import net.luminis.tls.handshake.TlsMessageParser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public class CryptoStream {
    private static final String TAG = CryptoStream.class.getSimpleName();
    private final Version quicVersion;
    private final EncryptionLevel encryptionLevel;
    private final ProtectionKeysType tlsProtectionType;
    private final Role peerRole;
    private final TlsEngine tlsEngine;
    private final Sender sender;
    private final TlsMessageParser tlsMessageParser;
    private final Queue<ByteBuffer> sendQueue = new ConcurrentLinkedDeque<>();
    private final AtomicInteger dataToSendOffset = new AtomicInteger(0);
    private final AtomicInteger sendStreamSize = new AtomicInteger(0);
    private final SortedSet<CryptoFrame> frames = new TreeSet<>();
    private boolean msgSizeRead = false; // no concurrency
    private int msgSize; // no concurrency
    private byte msgType; // no concurrency
    private long processedToOffset = 0; // no concurrency

    public CryptoStream(Version quicVersion, EncryptionLevel encryptionLevel, Role role,
                        TlsEngine tlsEngine, Sender sender) {
        this.quicVersion = quicVersion;
        this.encryptionLevel = encryptionLevel;
        this.peerRole = role.other();
        this.tlsEngine = tlsEngine;
        this.sender = sender;

        this.tlsProtectionType =
                encryptionLevel == EncryptionLevel.Handshake ? ProtectionKeysType.Handshake :
                        encryptionLevel == EncryptionLevel.App ? ProtectionKeysType.Application :
                                ProtectionKeysType.None;

        this.tlsMessageParser = new TlsMessageParser(this::quicExtensionsParser);
    }

    public void add(CryptoFrame cryptoFrame) throws TlsProtocolException {
        try {
            if (addFrame(cryptoFrame)) {
                long availableBytes = bytesAvailable();
                // Because the stream may not have enough bytes available to read the whole message, but enough to
                // read the size, the msg size read must be remembered for the next invocation of this method.
                // So, when this method is called, either one of the following cases holds:
                // - msg size was read last time, but not the message
                // - no msg size and (thus) no msg was read last time (or both where read, leading to the same state)
                // The boolean msgSizeRead is used to differentiate these two cases.
                while (msgSizeRead && availableBytes >= msgSize || !msgSizeRead && availableBytes >= 4) {
                    if (!msgSizeRead) {
                        // Determine message length (a TLS Handshake message starts with 1 byte type and 3 bytes length)
                        ByteBuffer buffer = ByteBuffer.allocate(4);
                        read(buffer);
                        msgType = buffer.get(0);
                        buffer.put(0, (byte) 0);  // Mask 1st byte as it contains the TLS handshake msg type
                        buffer.flip();
                        msgSize = buffer.getInt();
                        msgSizeRead = true;
                        availableBytes -= 4;
                    }
                    if (availableBytes >= msgSize) {
                        ByteBuffer msgBuffer = ByteBuffer.allocate(4 + msgSize);
                        msgBuffer.putInt(msgSize);
                        msgBuffer.put(0, msgType);
                        int read = read(msgBuffer);
                        availableBytes -= read;
                        msgSizeRead = false;

                        msgBuffer.flip();

                        tlsMessageParser.parseAndProcessHandshakeMessage(msgBuffer,
                                tlsEngine, tlsProtectionType);

                        if (msgBuffer.hasRemaining()) {
                            throw new RuntimeException();  // Must be programming error
                        }
                    }
                }
            } else {
                LogUtils.debug(TAG, "Discarding " + cryptoFrame +
                        ", because stream already parsed to " + readOffset());
            }
        } catch (IOException e) {
            // Impossible, because the kwik implementation of the ClientMessageSender does not throw IOException.
            throw new RuntimeException(e);
        }
    }

    Extension quicExtensionsParser(ByteBuffer buffer, HandshakeType context) throws TlsProtocolException {
        buffer.mark();
        int extensionType = buffer.getShort();
        buffer.reset();
        if (QuicTransportParametersExtension.isCodepoint(quicVersion, extensionType & 0xffff)) {
            return new QuicTransportParametersExtension(quicVersion).parse(buffer, peerRole);
        } else {
            return null;
        }
    }

    @NonNull
    @Override
    public String toString() {
        return toStringWith(Collections.emptyList());
    }

    private String toStringWith(List<Message> messages) {
        return "CryptoStream[" + encryptionLevel.name().charAt(0) + "|" + messages.stream()
                .map(msg -> msg.getClass().getSimpleName())
                .map(name -> name.endsWith("Message") ? name.substring(0, name.length() - 7) : name)
                .collect(Collectors.joining(","))
                + "]";
    }

    public void write(HandshakeMessage message, boolean flush) {
        write(message.getBytes());
        if (flush) {
            sender.flush();
        }
    }

    private void write(byte[] data) {
        sendQueue.add(ByteBuffer.wrap(data));
        sendStreamSize.getAndAdd(data.length);
        sender.send(this::sendFrame, 10, encryptionLevel, this::retransmitCrypto);  // Caller should flush sender.
    }

    private QuicFrame sendFrame(int maxSize) {
        int leftToSend = sendStreamSize.get() - dataToSendOffset.get();
        int bytesToSend = Integer.min(leftToSend, maxSize - 10);
        if (bytesToSend == 0) {
            return null;
        }
        if (bytesToSend < leftToSend) {
            // Need (at least) another frame to send all data. Because current method is the sender callback, flushing sender is not necessary.
            sender.send(this::sendFrame, 10, encryptionLevel, this::retransmitCrypto);
        }

        byte[] frameData = new byte[bytesToSend];
        int frameDataOffset = 0;
        while (frameDataOffset < bytesToSend && !sendQueue.isEmpty()) {
            ByteBuffer buffer = sendQueue.peek();
            if (buffer != null) {
                int bytesToCopy = Integer.min(bytesToSend - frameDataOffset, buffer.remaining());
                buffer.get(frameData, frameDataOffset, bytesToCopy);
                if (buffer.remaining() == 0) {
                    sendQueue.poll();
                }
                frameDataOffset += bytesToCopy;
            }
        }

        CryptoFrame frame = new CryptoFrame(quicVersion, dataToSendOffset.get(), frameData);
        dataToSendOffset.getAndAdd(bytesToSend);
        return frame;
    }

    private void retransmitCrypto(QuicFrame cryptoFrame) {
        LogUtils.verbose(TAG, "Retransmitting " + cryptoFrame +
                " on level " + encryptionLevel);
        sender.send(cryptoFrame, encryptionLevel, this::retransmitCrypto);
    }

    public void reset() {
        dataToSendOffset.set(0);
        sendStreamSize.set(0);
        sendQueue.clear();
    }

    /**
     * Add a stream frame to this stream. The frame can contain any number of bytes positioned anywhere in the stream;
     * the read method will take care of returning stream bytes in the right order, without gaps.
     *
     * @return true if the frame is adds bytes to this stream; false if the frame does not add bytes to the stream
     * (because the frame is a duplicate or its stream bytes where already received with previous frames).
     */
    protected boolean addFrame(CryptoFrame frame) {
        if (frame.getUpToOffset() > processedToOffset) {
            frames.add(frame);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the number of bytes that can be read from this stream.
     */
    protected int bytesAvailable() {
        if (frames.isEmpty()) {
            return 0;
        } else {
            int available = 0;
            long countedUpTo = processedToOffset;

            for (CryptoFrame nextFrame : frames) {
                if (nextFrame.getOffset() <= countedUpTo) {
                    if (nextFrame.getUpToOffset() > countedUpTo) {
                        available += nextFrame.getUpToOffset() - countedUpTo;
                        countedUpTo = nextFrame.getUpToOffset();
                    }
                } else {
                    break;
                }
            }
            return available;
        }

    }


    /**
     * Read a much as possible bytes from the stream (limited by the size of the given buffer or the number of bytes
     * available on the stream). If no byte is available because the end of the stream has been reached, the value -1 is returned.
     * Does not block: returns 0 when no bytes can be read.
     */
    protected int read(ByteBuffer buffer) {

        if (frames.isEmpty()) {
            return 0;
        } else {
            int read = 0;
            long readUpTo = processedToOffset;
            Iterator<CryptoFrame> iterator = frames.iterator();

            while (iterator.hasNext() && buffer.remaining() > 0) {
                CryptoFrame nextFrame = iterator.next();
                if (nextFrame.getOffset() <= readUpTo) {
                    if (nextFrame.getUpToOffset() > readUpTo) {
                        long available = nextFrame.getOffset() - readUpTo + nextFrame.getLength();
                        int bytesToRead = (int) Long.min(buffer.limit() - buffer.position(), available);
                        buffer.put(nextFrame.getStreamData(), (int) (readUpTo - nextFrame.getOffset()), bytesToRead);
                        readUpTo += bytesToRead;
                        read += bytesToRead;
                    }
                } else {
                    break;
                }
            }

            processedToOffset += read;
            removeParsedFrames();
            return read;
        }

    }


    private void removeParsedFrames() {
        Iterator<CryptoFrame> iterator = frames.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getUpToOffset() <= processedToOffset) {
                iterator.remove();
            } else {
                break;
            }
        }
    }

    /**
     * Returns the position in the stream up to where stream bytes are read.
     */
    private long readOffset() {
        return processedToOffset;
    }
}
